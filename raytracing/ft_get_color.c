/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_colors.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bkany <bkany@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/26 13:37:07 by bkany             #+#    #+#             */
/*   Updated: 2020/07/26 13:44:13 by bkany            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../minirt.h"



/*
** CALCULATE THE COLOR OF ONE PIXEL ON THE FIG DEPENDING OF SPOTS IN THE SCENE
*/

int			ft_get_color_fig(t_color *col, t_solve *solve, t_set *set)
{
	float			r;
	float			g;
	float			b;
	float			t;
	float			intensity_light;
	float			intensity_pixel;
	t_vect			*vect_to_light;
	float			prodscal;
	t_plus_light	*spot;
	int				shadow;

	intensity_light = set->light1->r * 1500000;
	spot = set->light2;
	vect_to_light = NULL;
	intensity_pixel = 0;
	shadow = 1;
	prodscal = 0;
	r = 0;
	g = 0;
	b = 0;
	t = 0;
//	r = col->r;
//	g = col->g;
//	b = col->b;

	while (spot != NULL)
	{
		vect_to_light = ft_vect(solve->inter, spot->coord);
		ft_vectunit(vect_to_light);
		prodscal = ft_prodscal(vect_to_light, solve->normale);
		intensity_pixel = intensity_light * ft_maxf(0, prodscal) / ft_dist2(solve->inter, spot->coord);
		shadow = ft_inshadow(set->light2, set, solve);
		r = r + col->r * intensity_pixel * shadow / 255;
		g = g + col->g * intensity_pixel * shadow / 255;
		b = b + col->b * intensity_pixel * shadow / 255;
		r = ft_minf(255, ft_maxf(0, r));
		g = ft_minf(255, ft_maxf(0, g));
		b = ft_minf(255, ft_maxf(0, b));

		spot = spot->next;
	}
	return ((int)(r) * 256 * 256 + (int)(g) * 256 + (int)(b));
}

int			ft_get_color(t_solve *solve, t_set *set)
{
	if (solve->type == 0)
		return (ft_get_color_fig(ft_get_sphere(solve, set)->color, solve, set));
	if (solve->type == 1)
		return (ft_get_color_fig(ft_get_plan(solve, set)->color, solve, set));
	if (solve->type == 2)
		return (ft_get_color_fig(ft_get_square(solve, set)->color, solve, set));
	if (solve->type == 3)
		return (ft_get_color_fig(ft_get_cyl(solve, set)->color, solve, set));
	if (solve->type == 4)
		return (ft_get_color_fig(ft_get_triang(solve, set)->color, solve, set));
}

t_sphere	*ft_get_sphere(t_solve *solve, t_set *set)
{
	int				tmp;
	t_sphere		*sphere_tmp;

	tmp = 0;
	sphere_tmp = set->fig->sphere;
	while (sphere_tmp != NULL)
	{
		if (tmp == solve->num)
			return (sphere_tmp);
		tmp++;
		sphere_tmp = sphere_tmp->next;
	}
	return (NULL);
}

t_plan		*ft_get_plan(t_solve *solve, t_set *set)
{
	int			tmp;
	t_plan		*plan_tmp;

	tmp = 0;
	plan_tmp = set->fig->plan;
	while (plan_tmp != NULL)
	{
		if (tmp == solve->num)
			return (plan_tmp);
		tmp++;
		plan_tmp = plan_tmp->next;
	}
	return (NULL);
}

t_square	*ft_get_square(t_solve *solve, t_set *set)
{
	int			tmp;
	t_square	*square_tmp;

	tmp = 0;
	square_tmp = set->fig->square;
	while (square_tmp != NULL)
	{
		if (tmp == solve->num)
			return (square_tmp);
		tmp++;
		square_tmp = square_tmp->next;
	}
	return (NULL);
}

t_cyl		*ft_get_cyl(t_solve *solve, t_set *set)
{
	int			tmp;
	t_cyl		*cyl_tmp;

	tmp = 0;
	cyl_tmp = set->fig->cyl;
	while (cyl_tmp != NULL)
	{
		if (tmp == solve->num)
			return (cyl_tmp);
		tmp++;
		cyl_tmp = cyl_tmp->next;
	}
	return (NULL);
}

t_triang	*ft_get_triang(t_solve *solve, t_set *set)
{
	int			tmp;
	t_triang	*triang_tmp;

	tmp = 0;
	triang_tmp = set->fig->triang;
	while (triang_tmp != NULL)
	{
		if (tmp == solve->num)
			return (triang_tmp);
		tmp++;
		triang_tmp = triang_tmp->next;
	}
	return (NULL);
}
