/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tools.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bkany <bkany@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/26 13:37:07 by bkany             #+#    #+#             */
/*   Updated: 2020/07/26 13:44:13 by bkany            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../minirt.h"

void		ft_transl_cam(t_camera *cam, char c, int a)
{
	if (c == 'x')
		cam->coord->x = cam->coord->x + a;
	if (c == 'y')
		cam->coord->y = cam->coord->y + a;
	if (c == 'z')
		cam->coord->z = cam->coord->z + a;
}

void		ft_transl_light(t_plus_light *light, char c, int a)
{
	if (c == 'x')
		light->coord->x = light->coord->x + a;
	if (c == 'y')
		light->coord->y = light->coord->y + a;
	if (c == 'z')
		light->coord->z = light->coord->z + a;
}

void		ft_transl_sphere(t_sphere *sphere, char c, int a)
{
	if (c == 'x')
		sphere->coord->x = sphere->coord->x + a;
	if (c == 'y')
		sphere->coord->y = sphere->coord->y + a;
	if (c == 'z')
		sphere->coord->z = sphere->coord->z + a;
}

void		ft_transl_plan(t_plan *plan, char c, int a)
{
	if (c == 'x')
		plan->coord->x = plan->coord->x + a;
	if (c == 'y')
		plan->coord->y = plan->coord->y + a;
	if (c == 'z')
		plan->coord->z = plan->coord->z + a;
}

void		ft_transl_square(t_square *square, char c, int a)
{
	if (c == 'x')
		square->coord->x = square->coord->x + a;
	if (c == 'y')
		square->coord->y = square->coord->y + a;
	if (c == 'z')
		square->coord->z = square->coord->z + a;
}

void		ft_transl_cyl(t_sphere *sphere, char c, int a)
{
	if (c == 'x')
		cyl->coord->x = cyl->coord->x + a;
	if (c == 'y')
		cyl->coord->y = cyl->coord->y + a;
	if (c == 'z')
		cyl->coord->z = cyl->coord->z + a;
}
