/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tools.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bkany <bkany@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/26 13:37:07 by bkany             #+#    #+#             */
/*   Updated: 2020/07/26 13:44:13 by bkany            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../minirt.h"

t_vect		*ft_vect(t_coord *coord1, t_coord *coord2)
{
	t_vect	*result;
	float	x;
	float	y;
	float	z;

	x = coord2->x - coord1->x;
	y = coord2->y - coord1->y;
	z = coord2->z - coord1->z;
	result = ft_init_vect(x, y, z);
	return (result);
}

void		ft_set_vect(t_vect *v, float a, float b, float c)
{
	v->x = a;
	v->y = b;
	v->z = c;
}

t_vect		*ft_prodvect(t_vect *vect1, t_vect *vect2)
{
	t_vect	*result;
	float	x;
	float	y;
	float	z;

	x = vect1->y * vect2->z - vect1->z * vect2->y;
	y = vect1->z * vect2->x - vect1->x * vect2->z;
	z = vect1->x * vect2->y - vect1->y * vect2->x;
	result = ft_init_vect(x, y, z);
	return (result);
}

float		ft_prodscal(t_vect *vect1, t_vect *vect2)
{
	float	result;

	result = vect1->x * vect2->x + vect1->y * vect2->y
			+ vect1->z * vect2->z;
	return (result);
}

float		ft_norm(t_vect *vect)
{
	float	result;

	result = powf(vect->x, 2) + powf(vect->y, 2) + powf(vect->z, 2);
	result = sqrtf(result);
	return (result);
}

float		ft_norm2(t_vect *vect)
{
	float	result;

	result = powf(vect->x, 2) + powf(vect->y, 2) + powf(vect->z, 2);
	return (result);
}

t_vect		*ft_multvect(t_vect *vect, float a)
{
	t_vect	*result;

	vect->x = a * vect->x;
	vect->y = a * vect->y;
	vect->z = a * vect->z;
	result = ft_init_vect(vect->x, vect->y, vect->z);
	return (result);
}

t_vect		*ft_add_vect(t_vect *vect1, t_vect *vect2)
{
	t_vect	*result;
	float	x;
	float	y;
	float	z;

	x = vect1->x + vect2->x;
	y = vect1->y + vect2->y;
	z = vect1->z + vect2->z;
	result = ft_init_vect(x, y, z);
	return (result);
}

t_vect		*ft_sumvect(t_vect *vect1, t_vect *vect2)
{
	t_vect	*result;

	result = ft_init_vect(vect1->x + vect2->x, vect1->y + vect2->y,
		vect1->z + vect2->z);
	return (result);
}

float		ft_dist(t_coord *coord1, t_coord *coord2)
{
	float	result;

	result = powf((coord2->x - coord1->x), 2)
		+ powf((coord2->y - coord1->y), 2)
		+ powf((coord2->z - coord1->z), 2);
	result = sqrtf(result);
	return (result);
}

float		ft_dist2(t_coord *coord1, t_coord *coord2)
{
	float	result;

	result = powf((coord2->x - coord1->x), 2)
		+ powf((coord2->y - coord1->y), 2)
		+ powf((coord2->z - coord1->z), 2);
	return (result);
}

int			ft_isalign(t_coord *coord_a, t_coord *coord_b, t_coord *coord_c)
{
	t_vect	*vect_ab;
	t_vect	*vect_ac;
	float	prodscal;

	vect_ab = ft_vect(coord_a, coord_b);
	vect_ac = ft_vect(coord_a, coord_c);
	if ((prodscal = ft_prodscal(vect_ab, vect_ac)) < 0)
		prodscal = -prodscal;
	if (prodscal == ft_norm(vect_ab) * ft_norm(vect_ac))
		return (1);
	else
		return (0);
}

void		ft_vectunit(t_vect *vect)
{
	float	norm;

	norm = ft_norm(vect);
	vect->x = vect->x / norm;
	vect->y = vect->y / norm;
	vect->z = vect->z / norm;
}

t_vect		*ft_vectunit2(t_vect *vect)
{
	float	norm;
	t_vect	*result;

	result = NULL;
	norm = ft_norm(vect);
	if (norm != 0)
		result = ft_multvect(vect, 1 / norm);
	return (result);
}

float		ft_angle(t_vect *vect1, t_vect *vect2)
{
	float	result;

	result = ft_prodscal(vect1, vect2) / (ft_norm(vect1) * ft_norm(vect2));
	result = acosf(result);
	return (result);
}

float		ft_abs(float f)
{
	if (f > 0)
		return (f);
	else
		return (-f);
}

t_coord		*ft_transl_pt(t_coord *coord, t_vect *vect, float a)
{
	float	x;
	float	y;
	float	z;
	t_coord	*result;

	x = coord->x + vect->x * a;
	y = coord->y + vect->y * a;
	z = coord->z + vect->z * a;
	result = ft_init_coord(x, y, z);
	return (result);
}

t_vect		*ft_mirorvect(t_vect *i, t_vect *norm)
{
	t_vect	*result;

	result = ft_multvect(norm, -2 * ft_prodscal(i, norm));
	result = ft_add_vect(i, result);
	return (result);
}

