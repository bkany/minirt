/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_inshadow.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bkany <bkany@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/26 13:37:07 by bkany             #+#    #+#             */
/*   Updated: 2020/07/26 13:44:13 by bkany            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../minirt.h"

/*
** CALCULATE IF AN INTERSECTION POINT IS IN THE SHADOW OF ANOTHER OBJECT,
** DEPENDING OF THE POSITION OF THE PLUS_LIGHT SPOT
*/

int			ft_inshadow(t_plus_light *l, t_set *set, t_solve *solve)
{
	t_vect	*v_inter_to_light;
	t_solve	*solve_shadow;
	t_coord	*pt;

	pt = ft_transl_pt(solve->inter, solve->normale, 0.01);
	v_inter_to_light = ft_vect(pt, l->coord);
	ft_vectunit(v_inter_to_light);
	solve_shadow = ft_swipe_fig(set, pt, v_inter_to_light);
	if (solve_shadow == NULL)
		return (1);
	else if (ft_dist(solve->inter, l->coord) > ft_dist(solve->inter,
		solve_shadow->inter) &&
		ft_prodscal(v_inter_to_light, ft_vect(pt, solve_shadow->inter)) >= 0)
		return (0);
	else
		return (1);
}
