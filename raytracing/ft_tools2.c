/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tools.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bkany <bkany@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/26 13:37:07 by bkany             #+#    #+#             */
/*   Updated: 2020/07/26 13:44:13 by bkany            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../minirt.h"

t_vect		*ft_rotx_vect(t_vect *vect, float theta)
{
	t_vect	*result;
	float	x;
	float	y;
	float	z;

	x = vect->x;
	y = vect->y * cosf(theta) - sinf(theta) * vect->z;
	z = vect->y * sinf(theta) + cosf(theta) * vect->z;
	result = ft_init_vect(x, y, z);
	return (result);
}

t_vect		*ft_roty_vect(t_vect *vect, float theta)
{
	t_vect	*result;
	float	x;
	float	y;
	float	z;

	x = vect->x * cosf(theta) + sinf(theta) * vect->z;
	y = vect->y;
	z = -vect->x * sinf(theta) + cosf(theta) * vect->z;
	result = ft_init_vect(x, y, z);
	return (result);
}

t_vect		*ft_rotz_vect(t_vect *vect, float theta)
{
	t_vect	*result;
	float	x;
	float	y;
	float	z;

	x = vect->x * cosf(theta) - sinf(theta) * vect->y;
	y = vect->x * sinf(theta) + cosf(theta) * vect->y;
	z = vect->z;
	result = ft_init_vect(x, y, z);
	return (result);
}

t_coord		*ft_rotx_coord(t_coord *coord, float theta)
{
	t_coord	*result;
	float	x;
	float	y;
	float	z;

	x = coord->x;
	y = coord->y * cosf(theta) - sinf(theta) * coord->z;
	z = coord->y * sinf(theta) + cosf(theta) * coord->z;
	result = ft_init_coord(x, y, z);
	return (result);
}

t_coord		*ft_roty_coord(t_coord *coord, float theta)
{
	t_coord	*result;
	float	x;
	float	y;
	float	z;

	x = coord->x * cosf(theta) + sinf(theta) * coord->z;
	y = coord->y;
	z = -coord->x * sinf(theta) + cosf(theta) * coord->z;
	result = ft_init_coord(x, y, z);
	return (result);
}

t_coord		*ft_rotz_coord(t_coord *coord, float theta)
{
	t_coord	*result;
	float	x;
	float	y;
	float	z;

	x = coord->x * cosf(theta) - sinf(theta) * coord->y;
	y = coord->x * sinf(theta) + cosf(theta) * coord->y;
	z = coord->z;
	result = ft_init_coord(x, y, z);
	return (result);
}


void		ft_rot_lights(t_plus_light *light, float thetax, float thetay, float thetaz)
{
	t_plus_light		*light_tmp;

	light_tmp = light;
	while (light_tmp != NULL)
	{
		light_tmp->coord = ft_rotx_coord(light_tmp->coord, thetax);
		light_tmp->coord = ft_roty_coord(light_tmp->coord, thetay);
		light_tmp->coord = ft_rotz_coord(light_tmp->coord, thetaz);
		light_tmp = light_tmp->next;
	}
}

void		ft_rot_spheres(t_sphere *sphere, float thetax, float thetay, float thetaz)
{
	t_sphere		*sphere_tmp;

	sphere_tmp = sphere;
	while (sphere_tmp != NULL)
	{
		sphere_tmp->coord = ft_rotx_coord(sphere_tmp->coord, thetax);
		sphere_tmp->coord = ft_roty_coord(sphere_tmp->coord, thetay);
		sphere_tmp->coord = ft_rotz_coord(sphere_tmp->coord, thetaz);
		sphere_tmp = sphere_tmp->next;
	}
}


void		ft_rot_plans(t_plan *plan, float thetax, float thetay, float thetaz)
{
	t_plan			*plan_tmp;

	plan_tmp = plan;
	while (plan_tmp != NULL)
	{
		plan_tmp->coord = ft_rotx_coord(plan_tmp->coord, thetax);
		plan_tmp->vect = ft_rotx_vect(plan_tmp->vect, thetax);
		plan_tmp->coord = ft_roty_coord(plan_tmp->coord, thetay);
		plan_tmp->vect = ft_roty_vect(plan_tmp->vect, thetay);
		plan_tmp->coord = ft_rotz_coord(plan_tmp->coord, thetaz);
		plan_tmp->vect = ft_rotz_vect(plan_tmp->vect, thetaz);
		plan_tmp = plan_tmp->next;
	}
}

void		ft_rot_squares(t_square *square, float thetax, float thetay, float thetaz)
{
	t_square			*square_tmp;

	square_tmp = square;
	while (square_tmp != NULL)
	{
		square_tmp->coord = ft_rotx_coord(square_tmp->coord, thetax);
		square_tmp->vect = ft_rotx_vect(square_tmp->vect, thetax);
		square_tmp->coord = ft_roty_coord(square_tmp->coord, thetay);
		square_tmp->vect = ft_roty_vect(square_tmp->vect, thetay);
		square_tmp->coord = ft_rotz_coord(square_tmp->coord, thetaz);
		square_tmp->vect = ft_rotz_vect(square_tmp->vect, thetaz);
		square_tmp = square_tmp->next;
	}
}

void		ft_rot_cyls(t_cyl *cyl, float thetax, float thetay, float thetaz)
{
	t_cyl			*cyl_tmp;

	cyl_tmp = cyl;
	while (cyl_tmp != NULL)
	{
		cyl_tmp->coord = ft_rotx_coord(cyl_tmp->coord, thetax);
		cyl_tmp->vect = ft_rotx_vect(cyl_tmp->vect, thetax);
		cyl_tmp->coord = ft_roty_coord(cyl_tmp->coord, thetay);
		cyl_tmp->vect = ft_roty_vect(cyl_tmp->vect, thetay);
		cyl_tmp->coord = ft_rotz_coord(cyl_tmp->coord, thetaz);
		cyl_tmp->vect = ft_rotz_vect(cyl_tmp->vect, thetaz);
		cyl_tmp = cyl_tmp->next;
	}
}

void		ft_rot_triangles(t_triang *triang, float thetax, float thetay, float thetaz)
{
	t_triang			*triang_tmp;

	triang_tmp = triang;
	while (triang_tmp != NULL)
	{
		triang_tmp->coord1 = ft_rotx_coord(triang_tmp->coord1, thetax);
		triang_tmp->coord2 = ft_rotx_coord(triang_tmp->coord2, thetax);
		triang_tmp->coord3 = ft_rotx_coord(triang_tmp->coord3, thetax);
		triang_tmp->coord1 = ft_roty_coord(triang_tmp->coord1, thetay);
		triang_tmp->coord2 = ft_roty_coord(triang_tmp->coord2, thetay);
		triang_tmp->coord3 = ft_roty_coord(triang_tmp->coord3, thetay);
		triang_tmp->coord1 = ft_rotz_coord(triang_tmp->coord1, thetaz);
		triang_tmp->coord2 = ft_rotz_coord(triang_tmp->coord2, thetaz);
		triang_tmp->coord3 = ft_rotz_coord(triang_tmp->coord3, thetaz);
		triang_tmp = triang_tmp->next;
	}
}

void		ft_rot_fig(t_fig *fig, float thetax, float thetay, float thetaz)
{
	ft_rot_spheres(fig->sphere, thetax, thetay, thetaz);
	ft_rot_plans(fig->plan, thetax, thetay, thetaz);
	ft_rot_squares(fig->square, thetax, thetay, thetaz);
	ft_rot_cyls(fig->cyl, thetax, thetay, thetaz);
	ft_rot_triangles(fig->triang, thetax, thetay, thetaz); 
}

void		ft_rot_set(t_set *set, t_vect *vect)
{
	float thetax;
	float thetay;
	float thetaz;

	thetax = ft_angle(ft_init_vect(0, 0, -1), vect);
	thetay = ft_angle(ft_init_vect(0, 1, 0), vect);
	thetaz = ft_angle(ft_init_vect(1, 0, 0), vect);
	ft_rot_lights(set->light2, thetax, thetay, thetaz);
	ft_rot_fig(set->fig, thetax, thetay, thetaz);
}

//v_dir = ft_rotx_vect(v_dir, ft_angle(ft_init_vect(0, 0, -1), set->cam->vect));
//v_dir = ft_roty_vect(v_dir, ft_angle(ft_init_vect(0, 1, 0), set->cam->vect));
//v_dir = ft_rotz_vect(v_dir, ft_angle(ft_init_vect(1, 0, 0), set->cam->vect));
