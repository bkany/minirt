/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_intersect.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bkany <bkany@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/26 13:37:07 by bkany             #+#    #+#             */
/*   Updated: 2020/07/26 13:44:13 by bkany            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../minirt.h"

t_solve		*ft_init_solve()
{
	t_solve		*solve;

	if (!(solve = (t_solve *)malloc(sizeof(t_solve) * 1)))
		return (NULL);
	return (solve);
}

t_solve		*ft_fill_solve(t_coord *p, t_vect *n, float s1, float s2, int t, int nu)
{
	t_solve		*solve;

	solve = ft_init_solve();
	solve->inter = p;
	solve->normale = n;
	solve->t1 = s1;
	solve->t2 = s2;
	solve->type = t;
	solve->num = nu;
	return (solve);

}

void		ft_free_solve(t_solve *t)
{
	free(t->inter);
	free(t->normale);
	free(t);
}

//------------------------------------------------------------------------------

t_solve		*ft_swipe_fig(t_set *set, t_coord *cam_coord, t_vect *vect_dir_win)
{
	t_solve		*solve;
	t_solve		*solve_tmp;
	t_sphere	*sphere_tmp;
	t_plan		*plan_tmp;
	t_cyl		*cyl_tmp;
	t_triang	*triang_tmp;
	int		i;
/*
	t_square	*square_tmp;		
*/

	solve = NULL;
	solve_tmp = NULL;
	i = 0;
	sphere_tmp = set->fig->sphere;
	plan_tmp = set->fig->plan;
	cyl_tmp = set->fig->cyl;
	triang_tmp = set->fig->triang;
/*
	square_tmp = set->fig->square;
	
*/
	while (sphere_tmp != NULL)
	{
		if (solve == NULL)
		{
			solve = ft_inter_sphere(cam_coord, sphere_tmp, vect_dir_win);
			if (solve != NULL)
				solve->num = i;
			sphere_tmp = sphere_tmp->next;
			i++;
		}
		else
		{
			solve_tmp = ft_inter_sphere(cam_coord, sphere_tmp, vect_dir_win);
			if (solve_tmp != NULL)
				solve_tmp->num = i;

			if ((solve_tmp != NULL) && (solve_tmp->t1 < solve->t1))
				solve = solve_tmp;
	
			sphere_tmp = sphere_tmp->next;
			i++;
		}
	}


	while (plan_tmp != NULL)
	{
		i = 0;
		if (solve == NULL)
		{
			solve = ft_inter_plan(cam_coord, plan_tmp, vect_dir_win);
			if (solve != NULL)
				solve->num = i;
			plan_tmp = plan_tmp->next;
			i++;
		}
		else
		{
			solve_tmp = ft_inter_plan(cam_coord, plan_tmp, vect_dir_win);
			if (solve_tmp != NULL)
				solve_tmp->num = i;

			if ((solve_tmp != NULL) && (solve_tmp->t1 < solve->t1))
				solve = solve_tmp;
	
			plan_tmp = plan_tmp->next;
			i++;
		}
	}
/*
	while (set->fig->square != NULL)
	{
		i = 0;
		if (solve == NULL)
		{
			solve = ft_inter_square(cam, set->fig->square, vect_dir_win);
			solve_tmp->type = 2;
			solve_tmp->num = i;
			i++;
		}
		if (((solve_tmp = ft_inter_square(cam, square_tmp, vect_dir_win)) != NULL) && (solve_tmp->t2 < solve->t2))
		{	
			ft_free_solve(solve);
			solve = solve_tmp;
		}
		square_tmp = square_tmp->next;
	}
*/
	while (cyl_tmp != NULL)
	{
		i = 0;
		if (solve == NULL)
		{
			solve = ft_inter_cyl(cam_coord, cyl_tmp, vect_dir_win);
			if (solve != NULL)
				solve->num = i;
			cyl_tmp = cyl_tmp->next;
			i++;
		}
		else
		{
			solve_tmp = ft_inter_cyl(cam_coord, cyl_tmp, vect_dir_win);
			if (solve_tmp != NULL)
				solve_tmp->num = i;

			if ((solve_tmp != NULL) && (solve_tmp->t1 < solve->t1))
				solve = solve_tmp;
	
			cyl_tmp = cyl_tmp->next;
			i++;
		}
	}

	while (triang_tmp != NULL)
	{
		i = 0;
		if (solve == NULL)
		{
			solve = ft_inter_triang(cam_coord, triang_tmp, vect_dir_win);
			if (solve != NULL)
				solve->num = i;
			triang_tmp = triang_tmp->next;
			i++;
		}
		else
		{
			solve_tmp = ft_inter_triang(cam_coord, triang_tmp, vect_dir_win);
			if (solve_tmp != NULL)
				solve_tmp->num = i;

			if ((solve_tmp != NULL) && (solve_tmp->t1 < solve->t1))
				solve = solve_tmp;
	
			triang_tmp = triang_tmp->next;
			i++;
		}
	}
	return (solve);
}

//------------------------------------------------------------------------------

t_solve		*ft_inter_sphere(t_coord *cam_coord, t_sphere *sphere, t_vect *vect_dir_win)
{
	float a;
	float b;
	float c;
	float delta;
	float t1;
	float t2;
	t_vect *v;
	t_solve	*solve;
	t_coord	*p;
	t_vect	*n;

	v = ft_vect(sphere->coord, cam_coord);
	a = 1;
	b = 2 * ft_prodscal(vect_dir_win, v);
	c = powf(ft_norm(v), 2) - powf(sphere->diam / 2, 2);
	delta = powf(b, 2) - 4.0 * a * c;
	if (delta > 0)
	{
		t1 = (-b - sqrtf(delta)) / (2.0 * a);
		t2 = (-b + sqrtf(delta)) / (2.0 * a);
		if (t2 < 0)
			return (NULL);
		if (t1 > 0)
		{
			p = ft_init_coord(cam_coord->x + t1 * vect_dir_win->x, 
					cam_coord->y + t1 * vect_dir_win->y, 
					cam_coord->z + t1 * vect_dir_win->z);
			n = ft_vect(sphere->coord, p);
			ft_vectunit(n);
			solve = ft_fill_solve(p, n, t1, t2, 0, 0);
			return (solve);
		}
		else
		{
			p = ft_init_coord(cam_coord->x + t2 * vect_dir_win->x, 
					cam_coord->y + t2 * vect_dir_win->y, 
					cam_coord->z + t2 * vect_dir_win->z);
			n = ft_vect(sphere->coord, p);
			ft_vectunit(n);
			solve = ft_fill_solve(p, n, t1, t2, 0, 0);
			return (solve);
		}
	}
	return (NULL);
}

//-----------------------------------------------------------------------------------------------

t_solve		*ft_inter_plan(t_coord *cam_coord, t_plan *plan, t_vect *vect_dir_win)
{
	float	t1;
	t_vect	*v;
	t_vect	*n;
	t_solve	*solve;
	t_coord *p;
	
	v = ft_vect(cam_coord, plan->coord);
	n = plan->vect;
	if (ft_prodscal(n, vect_dir_win) == 0)
		return (NULL);
	else
	{
		t1 = ft_prodscal(n, v) / ft_prodscal(n, vect_dir_win);
		if (t1 > 0)
		{	
		//	printf("%f\n", t1);
			p = ft_init_coord(cam_coord->x + t1 * vect_dir_win->x, 
						cam_coord->y + t1 * vect_dir_win->y, 
						cam_coord->z + t1 * vect_dir_win->z);
			solve = ft_fill_solve(p, n, t1, t1, 1, 0);
			return (solve);
		}
		else
			return (NULL);
	}
}

//-----------------------------------------------------------------------------------------------

t_solve		*ft_inter_cyl(t_coord *cam_coord, t_cyl *cyl, t_vect *vect_dir_win)
{
	t_vect	*L;
	t_vect	*cam_ray;
	float	a;
	float	b;
	float	c;
	float	delta;
	float	t1;
	float	t2;
	t_coord	*p;
	t_coord	*o;
	t_vect	*n;
	t_solve	*solve;
	
	cyl->vect = ft_vectunit2(cyl->vect);
	cam_ray = ft_init_vect(cam_coord->x, cam_coord->y, cam_coord->z);
	L = ft_vect(cyl->coord, cam_coord);
	
	a = 1 - ft_prodscal(cyl->vect, vect_dir_win) * ft_prodscal(cyl->vect, vect_dir_win);
	b = 2 * (ft_prodscal(vect_dir_win, L) - ft_prodscal(cyl->vect, vect_dir_win) * ft_prodscal(cyl->vect, L));
	c = ft_norm2(L) - powf(cyl->diam / 2, 2) - powf( ft_prodscal(cyl->vect, L), 2);

	//a = 1 - pow((cyl->vect->x * vect_dir_win->x + cyl->vect->y * vect_dir_win->y + cyl->vect->z * vect_dir_win->z), 2) ;
	//b = 2 * ( (L->x * vect_dir_win->x + L->y * vect_dir_win->y + L->z * vect_dir_win->z) - (cyl->vect->x * vect_dir_win->x + cyl->vect->y * vect_dir_win->y + cyl->vect->z * vect_dir_win->z) * (L->x * cyl->vect->x + L->y * cyl->vect->y + L->z * cyl->vect->z) );
	//c = (L->x * L->x + L->y * L->y + L->z * L->z) - pow(cyl->diam / 2.0, 2) - (L->x * cyl->vect->x + L->y * cyl->vect->y + L->z * cyl->vect->z) * (L->x * cyl->vect->x + L->y * cyl->vect->y + L->z * cyl->vect->z);
	delta = powf(b, 2) - 4.0 * a * c;
	if (delta > 0)
	{
		t1 = (-b - sqrtf(delta)) / (2.0 * a);
		t2 = (-b + sqrtf(delta)) / (2.0 * a);
		if (t2 < 0)
			return (NULL);
		if (t1 > 0)
		{
			p = ft_init_coord(cam_coord->x + t1 * vect_dir_win->x, 
					cam_coord->y + t1 * vect_dir_win->y, 
					cam_coord->z + t1 * vect_dir_win->z);	
			n = ft_prodvect(ft_prodvect(cyl->vect, ft_vect(cyl->coord, p)), cyl->vect);
			ft_vectunit(n);
			solve = ft_fill_solve(p, n, t1, t2, 3, 0);
			return (solve);
		}
		else
		{
			p = ft_init_coord(cam_coord->x + t2 * vect_dir_win->x, 
					cam_coord->y + t2 * vect_dir_win->y, 
					cam_coord->z + t2 * vect_dir_win->z);
			n = ft_prodvect(ft_prodvect(cyl->vect, ft_vect(cyl->coord, p)), cyl->vect);
			ft_vectunit(n);
			solve = ft_fill_solve(p, n, t2, t2, 3, 0);
			return (solve);
		}
	}
	return (NULL);
}

//-----------------------------------------------------------------------------------------------

t_solve		*ft_inter_square(t_coord *cam_coord, t_square *square, t_vect *vect_dir_win)
{
	(void) cam_coord;
	(void) square;
	(void) vect_dir_win;
	return (NULL);
}

//-----------------------------------------------------------------------------------------------

t_solve		*ft_inter_triang(t_coord *cam_coord, t_triang *triang, t_vect *vect_dir_win)
{
	t_vect	*ba;
	t_vect	*ca;
	float	d;
	float	u;
	float	v;
	float	t;

	//composantes de Cam_coord - Coord1
	float	a;
	float	b;
	float	c;

	t_solve	*solve;
	t_coord	*p;
	t_vect	*n;
	
	ba = ft_vect(triang->coord1, triang->coord2);
	ca = ft_vect(triang->coord1, triang->coord3);
	a = cam_coord->x - triang->coord1->x;
	b = cam_coord->y - triang->coord1->y;
	c = cam_coord->z - triang->coord1->z;
	d =  ba->z*vect_dir_win->x*ca->y - ba->z*vect_dir_win->y*ca->x
		- ca->z*vect_dir_win->y*ba->x + ca->z*vect_dir_win->x*ba->y
		- vect_dir_win->z*ca->x*ba->y + vect_dir_win->z*ca->y*ba->x;
	if (d != 0)
	{
		u = (-vect_dir_win->y*ca->z*a + vect_dir_win->y*ca->x*c - ca->x*vect_dir_win->z*b + a*vect_dir_win->z*ca->y - vect_dir_win->x*ca->y*c + vect_dir_win->x*b*ca->z)/d;
		v = -(vect_dir_win->x*ba->y*c - vect_dir_win->z*ba->x*b + vect_dir_win->z*a*ba->y + vect_dir_win->x*ba->z*b + vect_dir_win->y*ba->x*c - vect_dir_win->y*a*ba->z)/d;
		t = -(-a*ba->z*ca->y + a*ba->y*ca->z + ba->x*ca->y*c - ba->x*b*ca->z - ca->x*ba->y*c + ca->x*ba->z*b)/d;
	}
	if (d != 0 && u > 0 && v > 0 && ((u + v) <= 1))
	{
		p = ft_init_coord(cam_coord->x + t * vect_dir_win->x, 
					cam_coord->y + t * vect_dir_win->y, 
					cam_coord->z + t * vect_dir_win->z);	
		n = ft_prodvect(ba, ca);
		ft_vectunit(n);
		solve = ft_fill_solve(p, n, t, t, 3, 0);
		return (solve);
	}
	return (NULL);
}
