/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_struct.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bkany <bkany@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/26 13:37:07 by bkany             #+#    #+#             */
/*   Updated: 2020/06/26 13:14:13 by bkany            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../minirt.h"
#include <stdio.h>

void	ft_print_cyl(t_cyl *cyl)
{
	int				i;
	t_cyl			*cyl2;

	i = 1;
	ft_printf("CYLINDRES--------------------\n");
	cyl2 = cyl;
	if (cyl == NULL)
		ft_printf("cylinder == NULL\n");
	else
	{
		while (cyl2)
		{
			ft_printf("cylindre %d----------\n", i++);
			ft_print_coord(cyl2->coord);
			ft_print_vect(cyl2->vect);
			printf("diametre = %f\n", cyl2->diam);
			printf("hauteur = %f\n", cyl2->h);
			ft_print_color(cyl2->color);
			cyl2 = cyl2->next;
		}
	}
}

void	ft_print_triang(t_triang *triang)
{
	int				i;
	t_triang		*triang2;

	i = 1;
	ft_printf("TRIANGLES--------------------\n");
	triang2 = triang;
	if (triang == NULL)
		ft_printf("triang == NULL\n");
	else
	{
		while (triang2)
		{
			ft_printf("triangle %d----------\n", i++);
			ft_print_coord(triang2->coord1);
			ft_print_coord(triang2->coord2);
			ft_print_coord(triang2->coord3);
			ft_print_color(triang2->color);
			triang2 = triang2->next;
		}
	}
}

void	ft_print_fig(t_fig *fig)
{
	ft_printf("FIGURES-------------------------\n");
	if (fig == NULL)
		ft_printf("figures == NULL\n");
	else
	{
		ft_print_sphere(fig->sphere);
		ft_print_plan(fig->plan);
		ft_print_square(fig->square);
		ft_print_cyl(fig->cyl);
		ft_print_triang(fig->triang);
	}
}
