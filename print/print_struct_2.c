/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_struct.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bkany <bkany@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/26 13:37:07 by bkany             #+#    #+#             */
/*   Updated: 2020/06/26 13:14:13 by bkany            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../minirt.h"
#include <stdio.h>

void	ft_print_camera(t_camera *cam)
{
	int				i;
	t_camera		*cam2;

	i = 1;
	ft_printf("CAMERAS-------------------------\n");
	if (cam == NULL)
		ft_printf("camera == NULL\n");
	else
	{
		cam2 = cam;
		while (cam2 != NULL)
		{
			ft_printf("camera %d----------\n", i++);
			ft_print_coord(cam2->coord);
			ft_print_vect(cam2->vect);
			ft_printf("fov = %d\n", cam2->fov);
			cam2 = cam2->next;
		}
	}
}

void	ft_print_lights(t_plus_light *lights)
{
	int				i;
	t_plus_light	*lights2;

	i = 1;
	ft_printf("ADDED LIGHTS--------------------\n");
	lights2 = lights;
	if (lights == NULL)
		ft_printf("added lights == NULL\n");
	else
	{
		while (lights2)
		{
			ft_printf("light %d----------\n", i++);
			ft_print_coord(lights2->coord);
			printf("range = %f\n", lights2->range);
			ft_print_color(lights2->color);
			lights2 = lights2->next;
		}
	}
}

void	ft_print_sphere(t_sphere *sphere)
{
	int				i;
	t_sphere		*sphere2;

	i = 1;
	ft_printf("SPHERES--------------------\n");
	sphere2 = sphere;
	if (sphere == NULL)
		ft_printf("sphere == NULL\n");
	else
	{
		while (sphere2)
		{
			ft_printf("sphere %d----------\n", i++);
			ft_print_coord(sphere2->coord);
			printf("diametre = %f\n", sphere2->diam);
			ft_print_color(sphere2->color);
			sphere2 = sphere2->next;
		}
	}
}

void	ft_print_plan(t_plan *plan)
{
	int				i;
	t_plan			*plan2;

	i = 1;
	ft_printf("PLANS--------------------\n");
	plan2 = plan;
	if (plan == NULL)
		ft_printf("plan == NULL\n");
	else
	{
		while (plan2)
		{
			ft_printf("plan %d----------\n", i++);
			ft_print_coord(plan2->coord);
			ft_print_vect(plan2->vect);
			ft_print_color(plan2->color);
			plan2 = plan2->next;
		}
	}
}

void	ft_print_square(t_square *square)
{
	int				i;
	t_square		*square2;

	i = 1;
	ft_printf("SQUARES--------------------\n");
	square2 = square;
	if (square == NULL)
		ft_printf("square == NULL\n");
	else
	{
		while (square2)
		{
			ft_printf("square %d----------\n", i++);
			ft_print_coord(square2->coord);
			ft_print_vect(square2->vect);
			printf("hauteur = %f\n", square2->h);
			ft_print_color(square2->color);
			square2 = square2->next;
		}
	}
}
