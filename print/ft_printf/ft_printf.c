/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bkany <bkany@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/12 20:09:01 by bkany             #+#    #+#             */
/*   Updated: 2020/05/28 23:10:32 by bkany            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/libftprintf.h"

static int		ft_printformat(t_chaine1 *format)
{
	int		a;

	a = 0;
	while (format->i < format->length &&
			(format->chaineinit)[format->i] != '%')
	{
		write(1, &format->chaineinit[format->i], 1);
		format->i++;
		a++;
	}
	return (a);
}

static int		ft_printvar(t_flags *flags)
{
	int			i;

	i = 0;
	while (i < flags->length)
	{
		write(1, &flags->result[i], 1);
		i++;
	}
	return (flags->length);
}

static void		ft_free(t_chaine1 *format, t_flags *flags)
{
	if (format != NULL)
	{
		free(format->chaineinit);
		free(format);
	}
	if (flags != NULL)
	{
		free(flags->result);
		free(flags);
	}
}

int				ft_printf(char *s, ...)
{
	t_chaine1	*format;
	t_flags		*flags;
	va_list		paraminfos;
	int			sum;

	sum = 0;
	format = ft_init1(s);
	va_start(paraminfos, s);
	while (format->i < format->length)
	{
		sum = sum + ft_printformat(format);
		if (format->chaineinit[format->i] == '%')
		{
			flags = ft_init2();
			ft_treatflags(format, flags, paraminfos);
			ft_treatvar(flags, paraminfos);
			sum = sum + ft_printvar(flags);
			ft_free(NULL, flags);
		}
	}
	ft_free(format, NULL);
	va_end(paraminfos);
	return (sum);
}
