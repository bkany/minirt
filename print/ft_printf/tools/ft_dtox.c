/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dtox.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bkany <bkany@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/06 13:37:07 by bkany             #+#    #+#             */
/*   Updated: 2020/05/23 01:06:15 by bkany            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/libftprintf.h"

static int		ft_nbdigit(unsigned long nb)
{
	int			n_digit;

	n_digit = 0;
	while (nb != 0)
	{
		nb = nb / 16;
		n_digit++;
	}
	return (n_digit);
}

char			*ft_dtox(unsigned int n, int base)
{
	int			size;
	char		*result;
	char		*strbase;

	if (base == 1)
		strbase = "0123456789ABCDEF";
	if (base == 0)
		strbase = "0123456789abcdef";
	if (n == 0)
		return (ft_strdup1("0"));
	size = ft_nbdigit((unsigned int)n);
	if (!(result = (char *)malloc(sizeof(char) * size)))
		return (NULL);
	result[size] = 0;
	while (size != 0)
	{
		result[size - 1] = strbase[n % 16];
		n = n / 16;
		size--;
	}
	return (result);
}

char			*ft_dtox_p(unsigned long n)
{
	int			size;
	char		*result;
	char		*base;

	base = "0123456789abcdef";
	if (n == 0)
		return (ft_strdup1("0x0"));
	size = ft_nbdigit(n) + 2;
	if (!(result = (char *)malloc(sizeof(char) * size)))
		return (NULL);
	result[size] = 0;
	while (size != 0)
	{
		result[size - 1] = base[n % 16];
		n = n / 16;
		size--;
	}
	result[0] = '0';
	result[1] = 'x';
	return (result);
}
