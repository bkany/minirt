/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlen.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bkany <bkany@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/06 13:37:07 by bkany             #+#    #+#             */
/*   Updated: 2020/02/17 10:48:10 by bkany            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/libftprintf.h"

int		ft_strlen_ft_printf(const char *s)
{
	int i;

	i = 0;
	if (s == NULL)
		return (i);
	while (s[i] != '\0')
		i++;
	return (i);
}

int		ft_strlen1(char *s)
{
	int i;

	i = 0;
	if (s == NULL)
		return (i);
	while (s[i] != 0)
		i++;
	return (i);
}

int		ft_strlen2(char *s, char c)
{
	int i;

	i = 0;
	if (s == NULL)
		return (i);
	while (s[i] != 0 || s[i] != c)
		i++;
	return (i);
}
