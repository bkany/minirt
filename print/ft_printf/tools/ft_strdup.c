/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bkany <bkany@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/06 13:37:07 by bkany             #+#    #+#             */
/*   Updated: 2020/05/21 01:51:56 by bkany            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/libftprintf.h"

char				*ft_strdup_ft_printf(const char *s)
{
	char			*result;
	unsigned long	i;
	unsigned long	n;

	i = 0;
	if (s == NULL)
		return (NULL);
	n = (unsigned long)ft_strlen_ft_printf(s);
	if (!(result = (char *)malloc(sizeof(char) * (n + 1))))
		return (NULL);
	while (i < n)
	{
		result[i] = s[i];
		i++;
	}
	result[i] = '\0';
	return (result);
}

char				*ft_strdup1(char *s)
{
	char			*result;
	unsigned long	i;
	unsigned long	n;

	i = 0;
	if (s == NULL)
		return (NULL);
	n = (unsigned long)ft_strlen1(s);
	if (!(result = (char *)malloc(sizeof(char) * (n + 1))))
		return (NULL);
	while (i < n)
	{
		result[i] = ((char *)s)[i];
		i++;
	}
	result[i] = '\0';
	return (result);
}

char				*ft_strdup2(char *s, char c)
{
	char			*result;
	int				i;
	unsigned long	n;

	i = 0;
	n = (unsigned long)ft_strlen2(s, c);
	if (!(result = (char *)malloc(sizeof(char) * (n + 1))))
		return (NULL);
	while (s[i] != 0)
	{
		result[i] = ((char *)s)[i];
		i++;
	}
	result[i] = '\0';
	return (result);
}

char				*ft_strdup3(t_flags *sflags, unsigned int val)
{
	unsigned int	i;
	char			*result;

	if (!(result = (char *)malloc(sizeof(char) * (val + 1))))
		return (NULL);
	i = 0;
	while (i < val)
	{
		result[i] = sflags->result[i];
		i++;
	}
	result[i] = 0;
	free(sflags->result);
	sflags->result = result;
	return (result);
}
