/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putchar.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bkany <bkany@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/06 13:37:07 by bkany             #+#    #+#             */
/*   Updated: 2020/04/17 13:19:49 by bkany            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/libftprintf.h"

char		*ft_putchar(int c)
{
	char	*result;

	if (c == '\0')
	{
		if (!(result = (char *)malloc(sizeof(char) * 2)))
			return (NULL);
		result[0] = 0;
		result[1] = 0;
	}
	else
	{
		if (!(result = (char *)malloc(sizeof(char) * 2)))
			return (NULL);
		result[0] = (char)c;
		result[1] = 0;
	}
	return (result);
}
