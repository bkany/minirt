/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa_uns_int.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bkany <bkany@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/06 13:37:07 by bkany             #+#    #+#             */
/*   Updated: 2020/05/02 17:42:18 by bkany            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/libftprintf.h"

static int		ft_nbdigit_uns_int(unsigned int nb)
{
	int			n_digit;

	n_digit = 0;
	while (nb >= 10)
	{
		nb = nb / 10;
		n_digit++;
	}
	n_digit++;
	return (n_digit);
}

static int		ft_power_uns_int(unsigned int n, int power)
{
	if (power == 0 && n == 0)
		return (0);
	if (power == 1)
		return (n);
	if (power == 0)
		return (1);
	else
		return (n * ft_power_uns_int(n, power - 1));
	return (0);
}

char			*ft_itoa_uns_int(unsigned int n)
{
	int			size;
	char		*result;
	int			i;

	i = 0;
	size = ft_nbdigit_uns_int(n);
	if (!(result = (char *)malloc(sizeof(char) * (size + 1))))
		return (NULL);
	size = ft_nbdigit_uns_int(n);
	while (size > 0)
	{
		result[i] = (n / ft_power_uns_int(10, size - 1)) + '0';
		n = n - (n / ft_power_uns_int(10, size - 1)) *
			ft_power_uns_int(10, size - 1);
		size--;
		i++;
	}
	result[i] = 0;
	return (result);
}
