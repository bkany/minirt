/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bkany <bkany@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/06 13:37:07 by bkany             #+#    #+#             */
/*   Updated: 2020/05/07 16:34:08 by bkany            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/libftprintf.h"

int					ft_atoi1(t_chaine1 *schaine1)
{
	int				n;

	n = 0;
	while (ft_isdigit((schaine1->chaineinit)[schaine1->i]) == 1)
	{
		n = n * 10 + (schaine1->chaineinit)[schaine1->i] - '0';
		schaine1->i++;
	}
	schaine1->i--;
	return (n);
}

int					ft_atoi3(char *s)
{
	int				i;
	int				n;
	int				sign;

	i = 0;
	n = 0;
	sign = 1;
	while (ft_isdigit(s[i]) != 1 && s[i] != '+' && s[i] != '-')
		i++;
	if (s[i] == '-' || s[i] == '+')
	{
		if (s[i] == '-')
			sign = -1;
		i++;
	}
	while (ft_isdigit(s[i]) == 1)
	{
		n = n * 10 + s[i] - '0';
		i++;
	}
	return (n * sign);
}

static int			ft_power(int n, int p)
{
	if (p == 0 && n == 0)
		return (0);
	if (p == 1)
		return (n);
	if (p == 0)
		return (1);
	else
		return (n * ft_power(n, p - 1));
}

static int			ft_abcdef(int n, char c, int i)
{
	int				nprime;

	nprime = 0;
	if (c == 'a' || c == 'A')
		nprime = n + ft_power(16, i) * 10;
	if (c == 'b' || c == 'B')
		nprime = n + ft_power(16, i) * 11;
	if (c == 'c' || c == 'C')
		nprime = n + ft_power(16, i) * 12;
	if (c == 'd' || c == 'D')
		nprime = n + ft_power(16, i) * 13;
	if (c == 'e' || c == 'E')
		nprime = n + ft_power(16, i) * 14;
	if (c == 'f' || c == 'F')
		nprime = n + ft_power(16, i) * 15;
	return (nprime);
}

int					ft_atoi4(char *s)
{
	int				n;
	int				i;
	int				j;

	i = 0;
	j = ft_strlen_ft_printf(s) - 1;
	n = 0;
	while (j >= 0)
	{
		if ((s[j] >= 'a' && s[j] <= 'f') || (s[j] >= 'A' && s[j] <= 'F'))
			n = ft_abcdef(n, s[j], i);
		else
			n = n + ft_power(16, i) * (s[j] - '0');
		j--;
		i++;
	}
	return (n);
}
