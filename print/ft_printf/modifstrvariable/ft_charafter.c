/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_charafter.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bkany <bkany@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/12 20:09:01 by bkany             #+#    #+#             */
/*   Updated: 2020/05/15 11:38:47 by bkany            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/libftprintf.h"

int					ft_charafter(t_flags *sflags, int size_width)
{
	char			*result;
	int				i;

	i = 0;
	if (size_width > sflags->length)
	{
		if (!(result = (char *)malloc(sizeof(char) * (size_width + 1))))
			return (-1);
		while (i < sflags->length)
		{
			result[i] = (sflags->result)[i];
			i++;
		}
		while (i < size_width)
			result[i++] = ' ';
		result[size_width] = 0;
		free(sflags->result);
		sflags->result = result;
		sflags->length = size_width;
		return (1);
	}
	return (0);
}
