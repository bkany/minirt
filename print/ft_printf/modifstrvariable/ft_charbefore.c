/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_charbefore.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bkany <bkany@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/12 20:09:01 by bkany             #+#    #+#             */
/*   Updated: 2020/05/22 00:33:39 by bkany            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/libftprintf.h"

int					ft_charbefore(t_flags *sflags, char c, int size_max)
{
	char			*result;
	int				i;
	int				j;
	int				size_s;

	i = 0;
	j = 0;
	size_s = sflags->length;
	if (!(result = (char *)malloc(sizeof(char) * (size_max + 1))))
		return (-1);
	while (i <= (size_max - (size_s + 1)))
		result[i++] = c;
	while (i < size_max)
	{
		result[i] = (sflags->result)[j];
		i++;
		j++;
	}
	result[size_max] = 0;
	free(sflags->result);
	sflags->result = result;
	sflags->length = size_max;
	return (1);
}

void				ft_plusbefore1(t_flags *sflags, char c)
{
	int				i;

	i = 0;
	while ((sflags->result[i] == '0') || (sflags->result[i] == ' '))
		i++;
	if (sflags->result[i - 1] == '0')
		sflags->result[i - 2] = c;
	else
		sflags->result[i - 1] = c;
}

void				ft_plusbefore2(t_flags *sflags, char c)
{
	sflags->result[0] = c;
}

void				ft_plusbefore3(t_flags *sflags, char c)
{
	int				i;

	i = 0;
	if (sflags->result[0] == ' ')
	{
		while (sflags->result[i] == ' ')
			i++;
		sflags->result[--i] = c;
	}
	else
		ft_onecharbefore(sflags, c);
}

int					ft_onecharbefore(t_flags *sflags, char c)
{
	char			*result;
	int				i;
	int				size_s;

	i = 0;
	size_s = sflags->length;
	if (!(result = (char *)malloc(sizeof(char) * (size_s + 1))))
		return (-1);
	result[i] = c;
	i++;
	while (i <= size_s)
	{
		result[i] = sflags->result[i - 1];
		i++;
	}
	result[i] = 0;
	free(sflags->result);
	sflags->result = result;
	sflags->length = i;
	return (1);
}
