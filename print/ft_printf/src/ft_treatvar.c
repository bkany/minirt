/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_treatvar.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bkany <bkany@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/06 13:37:07 by bkany             #+#    #+#             */
/*   Updated: 2020/05/23 01:02:42 by bkany            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/libftprintf.h"

static void		ft_treatvar1(t_flags *sflags, va_list pi)
{
	if (ft_findchar("cspdiuxX%", sflags->c) != 0)
		free(sflags->result);
	if (sflags->c == 'c')
		sflags->result = ft_putchar((int)va_arg(pi, int));
	if (sflags->c == 's')
		sflags->result = ft_strdup_ft_printf((char *)va_arg(pi, char *));
	if (sflags->c == 'p')
		sflags->result =
			ft_dtox_p((unsigned long)va_arg(pi, unsigned long));
	if (sflags->c == 'd')
		sflags->result = ft_itoa_ft_printf((long int)va_arg(pi, int));
	if (sflags->c == 'i')
		sflags->result = ft_itoa_ft_printf((int)va_arg(pi, int));
	if (sflags->c == 'u')
		sflags->result =
			ft_itoa_uns_int((unsigned int)va_arg(pi, unsigned int));
	if (sflags->c == 'x')
		sflags->result =
			ft_dtox((unsigned int)va_arg(pi, unsigned int), 0);
	if (sflags->c == 'X')
		sflags->result =
			ft_dtox((unsigned int)va_arg(pi, unsigned int), 1);
	if (sflags->c == '%')
		sflags->result = ft_putchar(37);
}

static void		ft_treatvar2(t_flags *sflags)
{
	sflags->length = ft_strlen1(sflags->result);
	if (sflags->c == 'c')
		ft_treatvarc(sflags);
	if (sflags->c == 's')
		ft_treatvars(sflags);
	if (sflags->c == 'p')
		ft_treatvarp(sflags);
	if (sflags->c == 'd')
		ft_treatvard(sflags);
	if (sflags->c == 'i')
		ft_treatvard(sflags);
	if (sflags->c == 'u')
		ft_treatvaru(sflags);
	if (sflags->c == 'x')
		ft_treatvarx(sflags);
	if (sflags->c == 'X')
		ft_treatvarx(sflags);
	if (sflags->c == '%')
		ft_treatvarpercent(sflags);
}

void			ft_treatvar(t_flags *sflags, va_list paraminfos)
{
	ft_treatvar1(sflags, paraminfos);
	ft_treatvar2(sflags);
}
