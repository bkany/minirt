/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_treatvarpu.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bkany <bkany@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/06 13:37:07 by bkany             #+#    #+#             */
/*   Updated: 2020/05/28 23:18:43 by bkany            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/libftprintf.h"

static void			ft_addzerobetween(t_flags *sflags)
{
	char		*result;
	int			i;
	int			j;

	if (!(result = (char *)malloc(sizeof(char) * (sflags->precisionval + 2))))
		result = NULL;
	if (result != NULL && sflags->result != NULL)
	{
		i = 2;
		j = 2;
		result[0] = sflags->result[0];
		result[1] = sflags->result[1];
		while (i < sflags->precisionval - sflags->length + 4)
			result[i++] = '0';
		while (j < sflags->length)
		{
			result[i] = sflags->result[j];
			i++;
			j++;
		}
		result[i] = 0;
		free(sflags->result);
		sflags->result = result;
		sflags->length = ft_strlen1(sflags->result);
	}
}

void				ft_treatvarp(t_flags *sflags)
{
	if (sflags->precision == 1 && sflags->result[0] == '0' &&
			sflags->result[1] == 'x' && sflags->result[2] == '0' &&
			sflags->result[3] == 0 && sflags->precisionval == 0)
	{
		free(sflags->result);
		sflags->result = ft_strdup1("0x");
		sflags->length = 2;
	}
	if (sflags->precision == 1 && sflags->precisionval > sflags->length)
		ft_addzerobetween(sflags);
	if (sflags->width > sflags->length && sflags->minus == 0)
	{
		if (sflags->zero == 0)
			ft_charbefore(sflags, ' ', sflags->width);
		else
			ft_charbefore(sflags, '0', sflags->width);
	}
	if (sflags->minus == 1)
		ft_charafter(sflags, sflags->width);
}

void				ft_treatvarpercent(t_flags *sflags)
{
	if (sflags->width > sflags->length && sflags->minus == 0)
	{
		if (sflags->zero == 0)
			ft_charbefore(sflags, ' ', sflags->width);
		else
			ft_charbefore(sflags, '0', sflags->width);
	}
	if (sflags->minus == 1)
		ft_charafter(sflags, sflags->width);
}
