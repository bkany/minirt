/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_treatvarcsx.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bkany <bkany@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/06 13:37:07 by bkany             #+#    #+#             */
/*   Updated: 2020/05/22 19:41:39 by bkany            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/libftprintf.h"

void			ft_treatvarc(t_flags *sflags)
{
	if (sflags->result[0] == 0)
		sflags->length = 1;
	if ((int)sflags->width > sflags->length)
	{
		if (sflags->minus == 0)
			ft_charbefore(sflags, ' ', (int)sflags->width);
		else if (sflags->minus == 1 && (int)sflags->width > 0)
			ft_charafter(sflags, (int)sflags->width);
	}
}

static void		ft_treatvarsnull(t_flags *sflags)
{
	if (sflags->result == NULL)
	{
		free(sflags->result);
		sflags->result = ft_strdup_ft_printf("(null)");
		sflags->length = 6;
	}
}

void			ft_treatvars(t_flags *sflags)
{
	ft_treatvarsnull(sflags);
	if (sflags->precision == 1)
	{
		if (sflags->precisionval == 0)
		{
			free(sflags->result);
			sflags->result = ft_strdup_ft_printf("");
			sflags->length = 0;
		}
		else if (sflags->precisionval > 0)
		{
			ft_strdup3(sflags, sflags->precisionval);
			sflags->length = ft_strlen1(sflags->result);
		}
	}
	if ((int)sflags->width > sflags->length)
	{
		if (sflags->minus == 0 && sflags->zero == 0)
			ft_charbefore(sflags, ' ', (int)sflags->width);
		if (sflags->minus == 0 && sflags->zero == 1)
			ft_charbefore(sflags, '0', (int)sflags->width);
		if (sflags->minus == 1)
			ft_charafter(sflags, (int)sflags->width);
	}
}

static void		ft_treatvarxzero(t_flags *sflags)
{
	free(sflags->result);
	sflags->result = ft_strdup_ft_printf("");
	sflags->length = 0;
}

void			ft_treatvarx(t_flags *sflags)
{
	if (sflags->precision == 1)
	{
		if ((int)sflags->precisionval > ft_strlen_ft_printf(sflags->result))
			ft_charbefore(sflags, '0', (int)sflags->precisionval);
		else if ((int)sflags->precisionval == 0 &&
				ft_atoi4(sflags->result) == 0)
			ft_treatvarxzero(sflags);
	}
	if ((int)sflags->width > sflags->length && sflags->minus == 0)
	{
		if (sflags->zero == 1)
		{
			if (sflags->precision == 1 && sflags->precisionval >= 0)
				ft_charbefore(sflags, ' ', (int)sflags->width);
			else if (sflags->precision == 1 && sflags->precisionval < 0)
				ft_charbefore(sflags, '0', (int)sflags->width);
			else if (sflags->precision == 0)
				ft_charbefore(sflags, '0', (int)sflags->width);
		}
		else
			ft_charbefore(sflags, ' ', (int)sflags->width);
	}
	if ((int)sflags->width > sflags->length && sflags->minus == 1)
		ft_charafter(sflags, sflags->width);
}
