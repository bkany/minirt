/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bkany <bkany@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/12 20:09:01 by bkany             #+#    #+#             */
/*   Updated: 2020/04/17 11:42:09 by bkany            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/libftprintf.h"

t_chaine1		*ft_init1(char *s)
{
	t_chaine1	*schaine1;
	char		*tmp;

	tmp = ft_strdup_ft_printf(s);
	if (!(schaine1 = (t_chaine1 *)malloc(sizeof(t_chaine1))))
		return (NULL);
	schaine1->chaineinit = tmp;
	schaine1->i = 0;
	schaine1->length = ft_strlen_ft_printf(s);
	return (schaine1);
}

t_flags			*ft_init2(void)
{
	t_flags		*sflags;

	if (!(sflags = (t_flags *)malloc(sizeof(t_flags))))
		return (NULL);
	sflags->c = 0;
	sflags->result = ft_strdup_ft_printf("");
	sflags->precisionval = 0;
	sflags->length = 0;
	sflags->width = 0;
	sflags->minus = 0;
	sflags->zero = 0;
	sflags->precision = 0;
	sflags->space = 0;
	sflags->plus = 0;
	return (sflags);
}
