/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fillflags.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bkany <bkany@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/06 13:37:07 by bkany             #+#    #+#             */
/*   Updated: 2020/05/13 22:09:36 by bkany            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/libftprintf.h"

static void		ft_treatchar(t_chaine1 *schaine1, t_flags *sflags)
{
	if (ft_findchar("cspdiuxX%", (schaine1->chaineinit)[schaine1->i]) == 1)
	{
		if ((schaine1->chaineinit)[schaine1->i] == 'c')
			sflags->c = 'c';
		else if ((schaine1->chaineinit)[schaine1->i] == 's')
			sflags->c = 's';
		else if ((schaine1->chaineinit)[schaine1->i] == 'p')
			sflags->c = 'p';
		else if ((schaine1->chaineinit)[schaine1->i] == 'd')
			sflags->c = 'd';
		else if ((schaine1->chaineinit)[schaine1->i] == 'i')
			sflags->c = 'i';
		else if ((schaine1->chaineinit)[schaine1->i] == 'u')
			sflags->c = 'u';
		else if ((schaine1->chaineinit)[schaine1->i] == 'x')
			sflags->c = 'x';
		else if ((schaine1->chaineinit)[schaine1->i] == 'X')
			sflags->c = 'X';
		else if ((schaine1->chaineinit)[schaine1->i] == '%')
			sflags->c = '%';
		schaine1->i++;
	}
}

static void		ft_flagsprecision(t_chaine1 *sch1, t_flags *sflg, va_list pinf)
{
	sflg->precision = 1;
	sch1->i++;
	if (ft_isdigit((sch1->chaineinit)[sch1->i]) == 1)
	{
		sflg->precisionval = ft_atoi1(sch1);
		sch1->i++;
	}
	else if ((sch1->chaineinit)[sch1->i] == '*')
	{
		sflg->precisionval = (int)va_arg(pinf, int);
		sch1->i++;
	}
}

static void		ft_fillflag(unsigned int *flag, int *i)
{
	*flag = 1;
	*i = *i + 1;
}

static void		ft_fillflagw(t_chaine1 *sch1, t_flags *sflg, int a)
{
	if (a >= 0)
	{
		sflg->width = a;
		sch1->i++;
	}
	else
	{
		sflg->width = -a;
		sflg->minus = 1;
		sch1->i++;
	}
}

void			ft_treatflags(t_chaine1 *sch1, t_flags *sflg, va_list pinfos)
{
	if (sch1->i + 1)
	{
		sch1->i++;
		while ((ft_findchar(CHARABIA1, (sch1->chaineinit)[sch1->i]) == 1) ||
				(ft_isdigit((sch1->chaineinit)[sch1->i]) == 1))
		{
			if ((sch1->chaineinit)[sch1->i] == '-')
				ft_fillflag(&(sflg->minus), &(sch1->i));
			else if ((sch1->chaineinit)[sch1->i] == '+')
				ft_fillflag(&(sflg->plus), &(sch1->i));
			else if ((sch1->chaineinit)[sch1->i] == ' ')
				ft_fillflag(&(sflg->space), &(sch1->i));
			else if ((sch1->chaineinit)[sch1->i] == '0')
				ft_fillflag(&(sflg->zero), &(sch1->i));
			else if ((sch1->chaineinit)[sch1->i] == '.')
				ft_flagsprecision(sch1, sflg, pinfos);
			else if ((sch1->chaineinit)[sch1->i] == '*')
				ft_fillflagw(sch1, sflg, (int)va_arg(pinfos, int));
			else if (ft_isdigit((sch1->chaineinit)[sch1->i]) == 1)
				ft_fillflagw(sch1, sflg, ft_atoi1(sch1));
			else
				break ;
		}
		ft_treatchar(sch1, sflg);
	}
}
