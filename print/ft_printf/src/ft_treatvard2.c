/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_treatvard.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bkany <bkany@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/06 13:37:07 by bkany             #+#    #+#             */
/*   Updated: 2020/05/28 23:30:30 by bkany            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/libftprintf.h"

static void		ft_treatvardnegativewidth(t_flags *sflags, int d)
{
	if (sflags->zero == 1 && sflags->precision == 1 &&
			sflags->precisionval >= 0)
		ft_charbefore(sflags, ' ', (int)sflags->width);
	if (sflags->zero == 1 && sflags->precision == 1 &&
			sflags->precisionval < 0)
	{
		free(sflags->result);
		sflags->result = ft_itoa_ft_printf(-d);
		sflags->length = ft_strlen1(sflags->result);
		ft_charbefore(sflags, '0', (int)sflags->width);
		ft_plusbefore2(sflags, '-');
		sflags->length = ft_strlen1(sflags->result);
	}
	if (sflags->zero == 1 && sflags->precision == 0)
	{
		free(sflags->result);
		sflags->result = ft_itoa_ft_printf(-d);
		sflags->length = ft_strlen1(sflags->result);
		ft_charbefore(sflags, '0', (int)sflags->width);
		ft_plusbefore2(sflags, '-');
	}
	if (sflags->zero == 0)
		ft_charbefore(sflags, ' ', (int)sflags->width);
}

void			ft_treatvardnegative(t_flags *sflags, int d)
{
	if (sflags->precision == 1 &&
			(int)sflags->precisionval > (sflags->length - 1))
	{
		free(sflags->result);
		sflags->result = ft_itoa_ft_printf(-d);
		sflags->length = ft_strlen1(sflags->result);
		ft_charbefore(sflags, '0', (int)sflags->precisionval);
		ft_onecharbefore(sflags, '-');
		sflags->length = ft_strlen1(sflags->result);
	}
	if ((int)sflags->width > sflags->length && sflags->minus == 0)
	{
		ft_treatvardnegativewidth(sflags, d);
	}
	if ((int)sflags->width > sflags->length && sflags->minus == 1)
		ft_charafter(sflags, sflags->width);
}
