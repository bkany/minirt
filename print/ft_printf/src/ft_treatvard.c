/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_treatvard.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bkany <bkany@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/06 13:37:07 by bkany             #+#    #+#             */
/*   Updated: 2020/05/28 23:30:30 by bkany            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/libftprintf.h"

static void			ft_treatplus(t_flags *sflags)
{
	if (sflags->precision == 1 && sflags->width > 0 &&
			sflags->minus == 0)
		ft_plusbefore3(sflags, '+');
	else if (sflags->width < sflags->length &&
			sflags->precision == 0)
		ft_onecharbefore(sflags, '+');
	else
	{
		if (sflags->precision == 1)
			ft_onecharbefore(sflags, '+');
		if (sflags->width > 0 && sflags->zero == 1)
			ft_plusbefore2(sflags, '+');
		if (sflags->width > 0 && sflags->zero == 0)
			ft_plusbefore1(sflags, '+');
	}
}

static void			ft_treatvardzerowidth(t_flags *sflags)
{
	if ((sflags->zero == 1 && sflags->precision == 0) ||
			(sflags->zero == 1 && sflags->precisionval < 0))
		ft_charbefore(sflags, '0', sflags->width);
	else if (sflags->zero == 1 && sflags->precision == 1)
		ft_charbefore(sflags, ' ', sflags->width);
	else if (sflags->zero == 0 && sflags->precision == 1)
		ft_charbefore(sflags, ' ', sflags->width);
	else if (sflags->zero == 0 && sflags->precision == 0)
		ft_charbefore(sflags, ' ', sflags->width);
}

static void			ft_treatvardpositive(t_flags *sflags)
{
	if (sflags->precision == 1 && sflags->precisionval > sflags->length)
		ft_charbefore(sflags, '0', sflags->precisionval);
	if (sflags->width > sflags->length && sflags->minus == 0)
	{
		if (sflags->zero == 1 && sflags->precision == 1 &&
				sflags->precisionval >= 0)
			ft_charbefore(sflags, ' ', sflags->width);
		if (sflags->zero == 1 && sflags->precision == 1 &&
				sflags->precisionval < 0)
			ft_charbefore(sflags, '0', sflags->width);
		if (sflags->zero == 1 && sflags->precision == 0)
			ft_charbefore(sflags, '0', sflags->width);
		if (sflags->zero == 0)
			ft_charbefore(sflags, ' ', sflags->width);
	}
	if (sflags->minus == 1 && sflags->plus == 0)
		ft_charafter(sflags, sflags->width);
	if (sflags->minus == 1 && sflags->plus == 1)
		ft_charafter(sflags, sflags->width - 1);
	if (sflags->space == 1 && sflags->result[0] != ' ')
		ft_charbefore(sflags, ' ', (sflags->length + 1));
	if (sflags->plus == 1)
		ft_treatplus(sflags);
}

static void			ft_treatvardzero(t_flags *sflags)
{
	if (sflags->precision == 1)
	{
		if (sflags->precisionval == 0)
		{
			free(sflags->result);
			sflags->result = ft_strdup_ft_printf("");
			sflags->length = 0;
		}
		if (sflags->precisionval > sflags->length)
			ft_charbefore(sflags, '0', sflags->precisionval);
	}
	if (sflags->width > sflags->length && sflags->minus == 0)
		ft_treatvardzerowidth(sflags);
	if (sflags->width > sflags->length && sflags->minus == 1)
	{
		if (sflags->plus == 0)
			ft_charafter(sflags, sflags->width);
		if (sflags->plus == 1)
			ft_charafter(sflags, sflags->width);
	}
	if (sflags->space == 1 && sflags->result[0] != ' ')
		ft_charbefore(sflags, ' ', (sflags->length + 1));
	if (sflags->plus == 1)
		ft_treatplus(sflags);
}

void				ft_treatvard(t_flags *sflags)
{
	int				d;

	d = ft_atoi3(sflags->result);
	if (d == 0)
		ft_treatvardzero(sflags);
	else if (d < 0)
		ft_treatvardnegative(sflags, d);
	else
		ft_treatvardpositive(sflags);
}
