/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_treatvarpu.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bkany <bkany@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/06 13:37:07 by bkany             #+#    #+#             */
/*   Updated: 2020/05/28 23:15:32 by bkany            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/libftprintf.h"

static void		ft_treatvaruwidth(t_flags *sflags)
{
	if (sflags->zero == 1 && sflags->precision == 1 &&
			sflags->precisionval >= 0)
		ft_charbefore(sflags, ' ', sflags->width);
	if (sflags->zero == 1 && sflags->precision == 1 &&
			sflags->precisionval < 0)
		ft_charbefore(sflags, '0', sflags->width);
	if (sflags->zero == 1 && sflags->precision == 0)
		ft_charbefore(sflags, '0', sflags->width);
	if (sflags->zero == 0)
		ft_charbefore(sflags, ' ', sflags->width);
}

static void		ft_treatvaruzero(t_flags *sflags)
{
	if (sflags->precision == 1)
	{
		if (sflags->precisionval > sflags->length)
			ft_charbefore(sflags, '0', sflags->precisionval);
		else if (sflags->precisionval == 0)
		{
			free(sflags->result);
			sflags->result = ft_strdup_ft_printf("");
			sflags->length = 0;
		}
	}
	if (sflags->width > sflags->length && sflags->minus == 0)
		ft_treatvaruwidth(sflags);
	if (sflags->minus == 1)
		ft_charafter(sflags, sflags->width);
}

static void		ft_treatvarunonzero(t_flags *sflags)
{
	if (sflags->precision == 1 && sflags->precisionval > sflags->length)
		ft_charbefore(sflags, '0', sflags->precisionval);
	if (sflags->width > sflags->length && sflags->minus == 0)
		ft_treatvaruwidth(sflags);
	if (sflags->minus == 1)
		ft_charafter(sflags, sflags->width);
	if (sflags->space == 1 && sflags->result[0] != ' ')
		ft_charbefore(sflags, ' ', sflags->length + 1);
}

void			ft_treatvaru(t_flags *sflags)
{
	if (ft_atoi3(sflags->result) == 0)
		ft_treatvaruzero(sflags);
	else
		ft_treatvarunonzero(sflags);
}
