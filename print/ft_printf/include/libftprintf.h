/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libftprintf.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bkany <bkany@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/15 15:46:06 by bkany             #+#    #+#             */
/*   Updated: 2020/05/29 00:31:14 by bkany            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFTPRINTF_H
# define LIBFTPRINTF_H

# include <stdlib.h>
# include <stdarg.h>
# include <stdio.h>
# include <unistd.h>

# define NBCOL     1024
# define CHARABIA1 "-+0. *"
# define CHARABIA2 "cspdiuxX%"

typedef struct		s_chaine1
{
	char			*chaineinit;
	int				i;
	int				length;
}					t_chaine1;

typedef struct		s_flags
{
	char			c;
	char			*result;
	int				precisionval;
	int				length;
	int				width;
	unsigned int	minus;
	unsigned int	zero;
	unsigned int	precision;
	unsigned int	space;
	unsigned int	plus;
}					t_flags;

t_chaine1			*ft_init1(char *s);
t_flags				*ft_init2(void);
void				ft_treatflags(t_chaine1 *schaine1,
						t_flags *sflags, va_list paraminfos);
void				ft_treatvar(t_flags *sflags, va_list paraminfos);
void				ft_treatvarc(t_flags *sflags);
void				ft_treatvars(t_flags *sflags);
void				ft_treatvarp(t_flags *sflags);
void				ft_treatvardnegative(t_flags *sflags, int d);
void				ft_treatvard(t_flags *sflags);
void				ft_treatvaru(t_flags *sflags);
void				ft_treatvarx(t_flags *sflags);
void				ft_treatvarpercent(t_flags *sflags);
int					ft_strlen_ft_printf(const char *s);
int					ft_strlen1(char *s);
int					ft_strlen2(char *s, char c);
int					ft_findchar(char *s, char c);
char				*ft_strdup_ft_printf(const char *s);
char				*ft_strdup1(char *s);
char				*ft_strdup2(char *s, char c);
char				*ft_strdup3(t_flags *sflags, unsigned int val);
int					ft_isdigit(int c);
int					ft_atoi1(t_chaine1 *schaine1);
int					ft_atoi3(char *s);
int					ft_atoi4(char *s);
char				*ft_putchar(int c);
char				*ft_itoa_ft_printf(long int n);
char				*ft_itoa_uns_int(unsigned int n);
char				*ft_dtox(unsigned int n, int base);
char				*ft_dtox_p(unsigned long n);
int					ft_charbefore(t_flags *sflags, char c, int size_max);
void				ft_plusbefore1(t_flags *sflags, char c);
void				ft_plusbefore2(t_flags *sflags, char c);
void				ft_plusbefore3(t_flags *sflags, char c);
int					ft_onecharbefore(t_flags *sflags, char c);
int					ft_charafter(t_flags *sflags, int size_width);
int					ft_printf(char *s, ...);

#endif
