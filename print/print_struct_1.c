/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_struct.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bkany <bkany@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/26 13:37:07 by bkany             #+#    #+#             */
/*   Updated: 2020/06/26 13:14:13 by bkany            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../minirt.h"
#include <stdio.h>

void	ft_print_coord(t_coord *coord)
{
	ft_printf("Coordonnees Point\n");
	if (coord != NULL)
		printf("x = %f / y = %f / z = %f\n", coord->x, coord->y, coord->z);
	else
		ft_printf("coord = NULL\n");
}

void	ft_print_vect(t_vect *vect)
{
	ft_printf("Coordonnees Vecteur\n");
	if (vect != NULL)
		printf("x = %f / y = %f / z = %f\n", vect->x, vect->y, vect->z);
	else
		ft_printf("vector = NULL\n");
}

void	ft_print_color(t_color *col)
{
	ft_printf("RGB color\n");
	if (col != NULL)
		ft_printf("r = %d / g = %d / b = %d\n", col->r, col->g, col->b);
	else
		ft_printf("color = NULL\n");
}

void	ft_print_size_image(t_size_image *size)
{
	ft_printf("SIZE IMAGE----------------------\n");
	if (size != NULL)
		ft_printf("size x = %d / sixe y = %d\n", size->x, size->y);
	else
		ft_printf("image size == NULL\n");
}

void	ft_print_ambiant_light(t_ambiant_light *light)
{
	ft_printf("AMBIANT LIGHT-------------------\n");
	if (light != NULL)
	{
		printf("range = %f\n", light->r);
		ft_print_color(light->color);
	}
	else
		ft_printf("ambiant light == NULL\n");
}
