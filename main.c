/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_main.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bkany <bkany@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/26 13:37:07 by bkany             #+#    #+#             */
/*   Updated: 2020/07/26 13:44:13 by bkany            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"

/*
static int		ft_exit(int i)
{
	(void)i;
	return (0);
}
*/

int main(int argc, char **argv)
{
	(void)	argc;
	t_set	*set;
	void	*mlx;
	void	*mlx_win;
	t_data	img;

	ft_printf("----------------------------------------------------------\n");
	set = ft_fill_set(argv[1]);


	if (set != NULL)
	{
	//	ft_print_set(set);
	//	free(set);
	//	ft_free_set(set);
	//	test_mlx(set);
	}

	
	mlx = mlx_init();
	mlx_win = mlx_new_window(mlx, set->win->x, set->win->y, "NevaWindow");
	img.img = mlx_new_image(mlx, set->win->x, set->win->y);
	img.addr = mlx_get_data_addr(img.img, &img.bits_per_pixel, &img.line_length, &img.endian);
	
	// REMPLISSAGE DE L'IMAGE EN FONCTION DE SET
	ft_fill_image(img, set);

	// AFFICHAGE
	mlx_put_image_to_window(mlx, mlx_win, img.img, 0, 0);
		//remplacer exit par ft_exit qui free tout et exit(0) à la fin
	mlx_hook(mlx_win, 33, (1L<<17), exit, (void *)0);
	mlx_loop(mlx);
	

}
