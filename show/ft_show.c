/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_show.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bkany <bkany@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/26 13:37:07 by bkany             #+#    #+#             */
/*   Updated: 2020/07/26 13:44:13 by bkany            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../minirt.h"

#ifndef M_PI
# define M_PI 3.1415926535
#endif


static void	my_mlx_pixel_put(t_data *data, int x, int y, int color)
{
	char	*dst;

	dst = data->addr + (y * data->line_length + x * (data->bits_per_pixel / 8));
	*(unsigned int*)dst = color;
}

void	ft_fill_image(t_data data, t_set *set)
{
	int	i;
	int	j;
	t_solve	*solve;
	t_vect	*v_dir;
	int 	color;
	float	angle;

	i = 0;
	j = 0;
	angle = set->cam->fov * M_PI / 180;
	v_dir = ft_init_vect(0, 0, 0);

	while (j < set->win->y)
	{
		while (i < set->win->x)
		{
			ft_set_vect(v_dir, i - set->win->x / 2, j - set->win->y / 2, -set->win->x / (2 * tanf(angle / 2)));
			ft_vectunit(v_dir);

			//v_dir = ft_rotx_vect(v_dir, ft_angle(ft_init_vect(0, 0, -1), set->cam->vect));
			//v_dir = ft_roty_vect(v_dir, ft_angle(ft_init_vect(0, 1, 0), set->cam->vect));
			//v_dir = ft_rotz_vect(v_dir, ft_angle(ft_init_vect(1, 0, 0), set->cam->vect));

			if ((solve = ft_swipe_fig(set, set->cam->coord, v_dir)) != NULL)
			{
				color = ft_get_color(solve, set);
				my_mlx_pixel_put(&data, i, set->win->y - j - 1, color);

			}
			else
				my_mlx_pixel_put(&data, i, j, 0x01111250);
			i++;
			//ft_free_solve(solve);
			//free(v_dir);s
		}
		i = 0;
		j++;
	}

}
