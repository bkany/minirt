/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parsing_1_size_light_cam.c                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bkany <bkany@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/26 13:37:07 by bkany             #+#    #+#             */
/*   Updated: 2020/06/26 13:14:13 by bkany            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../minirt.h"

t_size_image		*ft_fill_size(char **result)
{
	t_size_image	*size_image;

	size_image = NULL;
	if (result[1] != NULL && result[2] != NULL
		&& ft_str_isdigit(result[1]) == 1 && ft_str_isdigit(result[2]) == 1)
	{
		if (ft_atoi(result[1]) >= 0 && ft_atoi(result[2]) >= 0)
		{
			size_image = ft_init_size(ft_atoi(result[1]), ft_atoi(result[2]));
		}
	}
	return (size_image);
}

t_ambiant_light		*ft_fill_amb_light(char **result)
{
	char			**result2;
	t_color			*color;
	t_ambiant_light	*amb_light;

	amb_light = NULL;
	if (result[1] != NULL && result[2] != NULL)
	{
		result2 = ft_split(result[2], ',');
		if (ft_str_isdigit(result[1]) == 1 && result2[0] != NULL &&
			result2[1] != NULL && result2[2] != NULL &&
			ft_strstr_isdigit(result2) == 1)
		{
			color = ft_init_color(ft_atoi(result2[0]),
				ft_atoi(result2[1]), ft_atoi(result2[2]));
			amb_light = ft_init_light(ft_atof(result[1]), color);
		}
		ft_free_tabtab(result2);
	}
	return (amb_light);
}

t_camera			*ft_fill_cam(char **result)
{
	char			**result2;
	t_coord			*coo;
	t_vect			*vec;
	t_camera		*cam;

	cam = NULL;
	if (result[1] != NULL && result[2] != NULL && result[3] != NULL &&
		ft_str_isdigit(result[3]) == 1)
	{
		result2 = ft_split(result[1], ',');
		if (result2[0] != NULL && result2[1] != NULL && result2[2] != NULL &&
			ft_strstr_isdigit(result2) == 1)
		{
			coo = ft_init_coord(ft_atof(result2[0]), ft_atof(result2[1]),
				ft_atof(result2[2]));
			ft_free_tabtab(result2);
			result2 = ft_split(result[2], ',');
			if (result2[0] != NULL && result2[1] != NULL && result2[2] != NULL
				&& ft_strstr_isdigit(result2) == 1)
			{
				vec = ft_init_vect(ft_atof(result2[0]), ft_atof(result2[1]),
					ft_atof(result2[2]));
				cam = ft_init_cam(coo, vec, ft_atoi(result[3]));
			}
			ft_free_tabtab(result2);
		}
		else if (result2)
			ft_free_tabtab(result2);
	}
	return (cam);
}

static t_camera		*ft_lastcam(t_camera *cam)
{
	t_camera		*tmp;

	tmp = NULL;
	while (cam != NULL)
	{
		tmp = cam;
		cam = cam->next;
	}
	return (tmp);
}

int					ft_fill_cams(t_camera *cam, char **result)
{
	t_camera		*cam2;
	int				error;

	cam2 = ft_fill_cam(result);
	if (cam2 != NULL)
	{
		ft_lastcam(cam)->next = cam2;
		error = ft_check_vect(cam2->vect) * ft_check_fov(cam2->fov);
	}
	else
		error = 0;
	return (error);
}
