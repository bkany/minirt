/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parsing_5_squares.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bkany <bkany@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/26 13:37:07 by bkany             #+#    #+#             */
/*   Updated: 2020/06/26 13:14:13 by bkany            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../minirt.h"

t_square		*ft_fill_square(char **result)
{
	char		**result2;
	t_coord		*coo;
	t_vect		*vect;
	t_color		*col;
	t_square	*square;

	square = NULL;
	if (result[1] != NULL && result[2] != NULL && result[3] != NULL && ft_str_isdigit(result[3]) == 1)
	{
		result2 = ft_split(result[1], ',');
		if (result2[0] != NULL && result2[1] != NULL && result2[2] != NULL && ft_strstr_isdigit(result2) == 1)
		{
			coo = ft_init_coord(ft_atof(result2[0]), ft_atof(result2[1]), ft_atof(result2[2]));
			ft_free_tabtab(result2);
			result2 = ft_split(result[2], ',');
			if (result2[0] != NULL && result2[1] != NULL && result2[2] != NULL && ft_strstr_isdigit(result2) == 1)
			{		
				vect = ft_init_vect(ft_atof(result2[0]), ft_atof(result2[1]), ft_atof(result2[2]));
				ft_free_tabtab(result2);
				result2 = ft_split(result[4], ',');
				if (result2[0] != NULL && result2[1] != NULL && result2[2] != NULL && ft_strstr_isdigit(result2) == 1)
				{
					col = ft_init_color(ft_atoi(result2[0]), ft_atoi(result2[1]), ft_atoi(result2[2]));
					ft_free_tabtab(result2);
					square = ft_init_squ(coo, vect, ft_atof(result[3]), col);
				}
				else if (result2)
					ft_free_tabtab(result2);
			}
			else if (result2)
				ft_free_tabtab(result2);
		}
		else if (result2)
			ft_free_tabtab(result2);
	}
	return (square);
}

static t_square	*ft_lastsquare(t_square *square)
{
	t_square	*tmp;

	tmp = NULL;
	while (square != NULL)
	{
		tmp = square;
		square = square->next;
	}
	return (tmp);
}

int				ft_fill_squares(t_square *square, char **result)
{
	t_square	*square2;
	int			error;

	error = 0;
	square2 = ft_fill_square(result);
	if (square2 != NULL)
	{
		ft_lastsquare(square)->next = square2;
		error = ft_check_vect(square2->vect) * ft_check_positive(square2->h)
			* ft_check_color(square2->color);
	}
	return (error);
}
