/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parsing_3_spheres.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bkany <bkany@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/26 13:37:07 by bkany             #+#    #+#             */
/*   Updated: 2020/06/26 13:14:13 by bkany            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../minirt.h"

t_sphere		*ft_fill_sphere(char **result)
{
	char		**result2;
	t_coord		*coo;
	t_color		*col;
	t_sphere	*sphere;

	sphere = NULL;
	col = NULL;
	coo = NULL;
	if (result[1] != NULL && result[2] != NULL && result[3] != NULL &&
		ft_str_isdigit(result[2]) == 1)
	{
		result2 = ft_split(result[1], ',');
		if (result2[0] != NULL && result2[1] != NULL && result2[2] != NULL &&
			ft_strstr_isdigit(result2) == 1)
		{
			coo = ft_init_coord(ft_atof(result2[0]), ft_atof(result2[1]),
				ft_atof(result2[2]));
			ft_free_tabtab(result2);
			result2 = ft_split(result[3], ',');
			if (result2[0] != NULL && result2[1] != NULL &&
				result2[2] != NULL && ft_strstr_isdigit(result2) == 1)
			{
				col = ft_init_color(ft_atoi(result2[0]), ft_atoi(result2[1]),
					ft_atoi(result2[2]));
				ft_free_tabtab(result2);
				sphere = ft_init_sphere(coo, ft_atof(result[2]), col);
			}
			else if (result2)
				ft_free_tabtab(result2);
		}
		else if (result2)
			ft_free_tabtab(result2);
	}
	return (sphere);
}

static t_sphere	*ft_lastsphere(t_sphere *sphere)
{
	t_sphere	*tmp;

	tmp = NULL;
	while (sphere != NULL)
	{
		tmp = sphere;
		sphere = sphere->next;
	}
	return (tmp);
}

int				ft_fill_spheres(t_sphere *sphere, char **result)
{
	t_sphere	*sphere2;
	int			error;

	error = 0;
	sphere2 = ft_fill_sphere(result);
	if (sphere2 != NULL)
	{
		ft_lastsphere(sphere)->next = sphere2;
		error = ft_check_color(sphere2->color) *
			ft_check_positive(sphere2->diam);
	}
	return (error);
}
