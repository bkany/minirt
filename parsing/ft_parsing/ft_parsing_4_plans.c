/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parsing_4_plans.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bkany <bkany@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/26 13:37:07 by bkany             #+#    #+#             */
/*   Updated: 2020/06/26 13:14:13 by bkany            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../minirt.h"

t_plan			*ft_fill_plan(char **result)
{
	char		**result2;
	t_coord		*coo;
	t_vect		*vect;
	t_color		*col;
	t_plan		*plan;

	plan = NULL;
	if (result[1] != NULL && result[2] != NULL && result[3] != NULL)
	{
		result2 = ft_split(result[1], ',');
		if (result2[0] != NULL && result2[1] != NULL && result2[2] != NULL && ft_strstr_isdigit(result2) == 1)
		{
			coo = ft_init_coord(ft_atof(result2[0]), ft_atof(result2[1]), ft_atof(result2[2]));
			ft_free_tabtab(result2);
			result2 = ft_split(result[2], ',');
			if (result2[0] != NULL && result2[1] != NULL && result2[2] != NULL && ft_strstr_isdigit(result2) == 1)
			{
				vect = ft_init_vect(ft_atof(result2[0]), ft_atof(result2[1]), ft_atof(result2[2]));
				ft_free_tabtab(result2);
				result2 = ft_split(result[3], ',');
				if (result2[0] != NULL && result2[1] != NULL && result2[2] != NULL && ft_strstr_isdigit(result2) == 1)
				{	
					col = ft_init_color(ft_atoi(result2[0]), ft_atoi(result2[1]), ft_atoi(result2[2]));
					ft_free_tabtab(result2);
					plan = ft_init_plan(coo, vect, col);
				}
				else if (result2)
					ft_free_tabtab(result2);
			}
			else if (result2)
				ft_free_tabtab(result2);
		}
		else if (result2)
			ft_free_tabtab(result2);
	}
	return (plan);
}

static t_plan	*ft_lastplan(t_plan *plan)
{
	t_plan		*tmp;

	tmp = NULL;
	while (plan != NULL)
	{
		tmp = plan;
		plan = plan->next;
	}
	return (tmp);
}

int				ft_fill_plans(t_plan *plan, char **result)
{
	t_plan		*plan2;
	int			error;

	error = 0;
	plan2 = ft_fill_plan(result);
	if (plan2 != NULL)
	{
		ft_lastplan(plan)->next = plan2;
		error = ft_check_vect(plan2->vect) * ft_check_color(plan2->color);
	}
	return (error);
}
