/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parsing_6_cylindres.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bkany <bkany@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/26 13:37:07 by bkany             #+#    #+#             */
/*   Updated: 2020/06/26 13:14:13 by bkany            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../minirt.h"

t_cyl			*ft_fill_cyl(char **result)
{
	char		**result2;
	t_coord		*coo;
	t_vect		*vect;
	t_color		*col;
	t_cyl		*cyl;

	cyl = NULL;
	if (result[1] != NULL && result[2] != NULL && result[3] != NULL && result[4] != NULL 
		&& result[5] != NULL && ft_str_isdigit(result[3]) == 1 && ft_str_isdigit(result[4]) == 1)
	{
		result2 = ft_split(result[1], ',');
		if (result2[0] != NULL && result2[1] != NULL && result2[2] != NULL && ft_strstr_isdigit(result2) == 1)
		{
			coo = ft_init_coord(ft_atof(result2[0]), ft_atof(result2[1]), ft_atof(result2[2]));
			ft_free_tabtab(result2);
			result2 = ft_split(result[2], ',');
			if (result2[0] != NULL && result2[1] != NULL && result2[2] != NULL && ft_strstr_isdigit(result2) == 1)
			{
				vect = ft_init_vect(ft_atof(result2[0]), ft_atof(result2[1]), ft_atof(result2[2]));
				ft_free_tabtab(result2);
				result2 = ft_split(result[5], ',');
				if (result2[0] != NULL && result2[1] != NULL && result2[2] != NULL && ft_strstr_isdigit(result2) == 1)
				{
					col = ft_init_color(ft_atoi(result2[0]), ft_atoi(result2[1]), ft_atoi(result2[2]));
					ft_free_tabtab(result2);
					cyl = ft_init_cyl1(coo, vect, ft_atof(result[3]));
					ft_init_cyl2(cyl, ft_atof(result[4]), col);
				}
				else if (result2)
					ft_free_tabtab(result2);
			}
			else if (result2)
				ft_free_tabtab(result2);
		}
		else if (result2)
			ft_free_tabtab(result2);
	}
	return (cyl);
}

static t_cyl	*ft_lastcyl(t_cyl *cyl)
{
	t_cyl		*tmp;

	tmp = NULL;
	while (cyl != NULL)
	{
		tmp = cyl;
		cyl = cyl->next;
	}
	return (tmp);
}

int			ft_fill_cyls(t_cyl *cyl, char **result)
{
	t_cyl		*cyl2;
	int		error;

	error = 0;
	cyl2 = ft_fill_cyl(result);
	if (cyl2 != NULL)
	{
		ft_lastcyl(cyl)->next = cyl2;
		error = ft_check_vect(cyl2->vect) * ft_check_color(cyl2->color) *
			ft_check_positive(cyl2->diam) * ft_check_positive(cyl2->h);
	}
	return (error);
}
