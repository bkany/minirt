/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parsing_7_triangles.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bkany <bkany@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/26 13:37:07 by bkany             #+#    #+#             */
/*   Updated: 2020/06/26 13:14:13 by bkany            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../minirt.h"

t_triang		*ft_fill_triang(char **result)
{
	char		**result2;
	t_coord		*coo1;
	t_coord		*coo2;
	t_coord		*coo3;
	t_color		*col;
	t_triang	*triang;

	triang = NULL;
	if (result[1] != NULL && result[2] != NULL && result[3] != NULL)
	{
		result2 = ft_split(result[1], ',');
		if (result2[0] != NULL && result2[1] != NULL && result2[2] != NULL && ft_strstr_isdigit(result2) == 1)
		{
			coo1 = ft_init_coord(ft_atof(result2[0]), ft_atof(result2[1]), ft_atof(result2[2]));
			ft_free_tabtab(result2);
			result2 = ft_split(result[2], ',');
			if (result2[0] != NULL && result2[1] != NULL && result2[2] != NULL && ft_strstr_isdigit(result2) == 1)
			{
				coo2 = ft_init_coord(ft_atof(result2[0]), ft_atof(result2[1]), ft_atof(result2[2]));
				ft_free_tabtab(result2);
				result2 = ft_split(result[3], ',');
				if (result2[0] != NULL && result2[1] != NULL && result2[2] != NULL && ft_strstr_isdigit(result2) == 1)
				{
					coo3 = ft_init_coord(ft_atof(result2[0]), ft_atof(result2[1]), ft_atof(result2[2]));
					ft_free_tabtab(result2);
					result2 = ft_split(result[4], ',');
					if (result2[0] != NULL && result2[1] != NULL && result2[2] != NULL && ft_strstr_isdigit(result2) == 1)
					{
						col = ft_init_color(ft_atoi(result2[0]), ft_atoi(result2[1]), ft_atoi(result2[2]));
						ft_free_tabtab(result2);
						triang = ft_init_tri(coo1, coo2, coo3, col);
					}
					else if (result2)
						ft_free_tabtab(result2);
				}
				else if (result2)
					ft_free_tabtab(result2);
			}
			else if (result2)
				ft_free_tabtab(result2);
		}
		else if (result2)
			ft_free_tabtab(result2);					
	}
	return (triang);
}

static t_triang	*ft_lasttriang(t_triang *triang)
{
	t_triang	*tmp;

	tmp = NULL;
	while (triang != NULL)
	{
		tmp = triang;
		triang = triang->next;
	}
	return (tmp);
}

int			ft_fill_triangs(t_triang *triang, char **result)
{
	t_triang	*triang2;
	int		error;

	error = 0;
	triang2 = ft_fill_triang(result);
	if (triang2 != NULL)
	{
		ft_lasttriang(triang)->next = triang2;
		error = ft_check_color(triang2->color);
	}
	return (error);
}
