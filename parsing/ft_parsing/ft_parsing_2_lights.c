/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parsing_2_lights.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bkany <bkany@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/26 13:37:07 by bkany             #+#    #+#             */
/*   Updated: 2020/06/26 13:14:13 by bkany            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../minirt.h"

t_plus_light		*ft_fill_light(char **result)
{
	char			**result2;
	t_coord			*coo;
	t_color			*col;
	t_plus_light	*lights;

	lights = NULL;
	if (result[1] != NULL && result[2] != NULL && result[3] != NULL &&
		ft_str_isdigit(result[2]) == 1)
	{
		result2 = ft_split(result[1], ',');
		if (result2[0] != NULL && result2[1] != NULL && result2[2] != NULL &&
			ft_strstr_isdigit(result2) == 1)
		{
			coo = ft_init_coord(ft_atof(result2[0]), ft_atof(result2[1]),
				ft_atof(result2[2]));
			ft_free_tabtab(result2);
			result2 = ft_split(result[3], ',');
			if (result2[0] != NULL && result2[1] != NULL && result2[2] != NULL
				&& ft_strstr_isdigit(result2) == 1)
			{
				col = ft_init_color(ft_atoi(result2[0]), ft_atoi(result2[1]),
					ft_atoi(result2[2]));
				ft_free_tabtab(result2);
				lights = ft_init_light2(coo, ft_atof(result[2]), col);
			}
			else if (result2)
				ft_free_tabtab(result2);
		}
		else if (result2)
			ft_free_tabtab(result2);
	}
	return (lights);
}

static t_plus_light	*ft_lastlight(t_plus_light *lights)
{
	t_plus_light	*tmp;

	tmp = NULL;
	while (lights != NULL)
	{
		tmp = lights;
		lights = lights->next;
	}
	return (tmp);
}

int					ft_fill_lights(t_plus_light *lights, char **result)
{
	t_plus_light	*lights2;
	int				error;

	error = 0;
	lights2 = ft_fill_light(result);
	if (lights2 != NULL)
	{
		ft_lastlight(lights)->next = lights2;
		error = ft_check_ratio(lights2->range) *
			ft_check_color(lights2->color);
	}
	return (error);
}
