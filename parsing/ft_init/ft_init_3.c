/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bkany <bkany@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/26 13:37:07 by bkany             #+#    #+#             */
/*   Updated: 2020/06/26 13:14:13 by bkany            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../minirt.h"

t_cyl		*ft_init_cyl1(t_coord *pt, t_vect *v, float d)
{
	t_cyl		*cyl;

	if (!(cyl = (t_cyl *)malloc(sizeof(t_cyl) * 1)))
		return (NULL);
	cyl->coord = pt;
	cyl->vect = v;
	cyl->diam = d;
	return (cyl);
}

void		ft_init_cyl2(t_cyl *cyl, float l, t_color *col)
{
	cyl->h = l;
	cyl->color = col;
	cyl->next = NULL;
}

t_triang	*ft_init_tri(t_coord *c1, t_coord *c2, t_coord *c3,
			t_color *col)
{
	t_triang	*tri;

	if (!(tri = (t_triang *)malloc(sizeof(t_triang) * 1)))
		return (NULL);
	tri->coord1 = c1;
	tri->coord2 = c2;
	tri->coord3 = c3;
	tri->color = col;
	tri->next = NULL;
	return (tri);
}
