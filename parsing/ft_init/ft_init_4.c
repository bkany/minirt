/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bkany <bkany@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/26 13:37:07 by bkany             #+#    #+#             */
/*   Updated: 2020/06/26 13:14:13 by bkany            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../minirt.h"

t_fig		*ft_init_fig(void)
{
	t_fig	*fig;

	if (!(fig = (t_fig *)malloc(sizeof(t_fig) * 1)))
		return (NULL);
	fig->sphere = NULL;
	fig->plan = NULL;
	fig->square = NULL;
	fig->cyl = NULL;
	fig->triang = NULL;
	return (fig);
}

t_set		*ft_init_set(void)
{
	t_set	*set;

	if (!(set = (t_set *)malloc(sizeof(t_set) * 1)))
		return (NULL);
	set->win = NULL;
	set->light1 = NULL;
	set->cam = NULL;
	set->light2 = NULL;
	set->fig = NULL;
	return (set);
}
