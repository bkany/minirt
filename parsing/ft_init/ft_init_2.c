/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bkany <bkany@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/26 13:37:07 by bkany             #+#    #+#             */
/*   Updated: 2020/06/26 13:14:13 by bkany            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../minirt.h"

t_camera			*ft_init_cam(t_coord *pov, t_vect *vect_o, int fov)
{
	t_camera			*cam;

	if (!(cam = (t_camera *)malloc(sizeof(t_camera) * 1)))
		return (NULL);
	cam->coord = pov;
	cam->vect = vect_o;
	cam->fov = fov;
	cam->next = NULL;
	return (cam);
}

t_plus_light		*ft_init_light2(t_coord *lum, float r, t_color *col)
{
	t_plus_light		*light2;

	if (!(light2 = (t_plus_light *)malloc(sizeof(t_plus_light) * 1)))
		return (NULL);
	light2->coord = lum;
	light2->range = r;
	light2->color = col;
	light2->next = NULL;
	return (light2);
}

t_sphere			*ft_init_sphere(t_coord *center, float d, t_color *col)
{
	t_sphere			*sphere;

	if (!(sphere = (t_sphere *)malloc(sizeof(t_sphere) * 1)))
		return (NULL);
	sphere->coord = center;
	sphere->diam = d;
	sphere->color = col;
	sphere->next = NULL;
	return (sphere);
}

t_plan				*ft_init_plan(t_coord *axe, t_vect *v, t_color *col)
{
	t_plan				*plan;

	if (!(plan = (t_plan *)malloc(sizeof(t_plan) * 1)))
		return (NULL);
	plan->coord = axe;
	plan->vect = v;
	plan->color = col;
	plan->next = NULL;
	return (plan);
}

t_square			*ft_init_squ(t_coord *pt, t_vect *v, float l, t_color *col)
{
	t_square	*square;

	if (!(square = (t_square *)malloc(sizeof(t_square) * 1)))
		return (NULL);
	square->coord = pt;
	square->vect = v;
	square->h = l;
	square->color = col;
	square->next = NULL;
	return (square);
}
