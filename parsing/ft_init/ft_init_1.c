/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_1.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bkany <bkany@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/26 13:37:07 by bkany             #+#    #+#             */
/*   Updated: 2020/06/26 13:14:13 by bkany            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../minirt.h"

t_coord					*ft_init_coord(float x, float y, float z)
{
	t_coord				*point;

	if (!(point = (t_coord *)malloc(sizeof(t_coord) * 1)))
		return (NULL);
	point->x = x;
	point->y = y;
	point->z = z;
	return (point);
}

t_vect					*ft_init_vect(float x, float y, float z)
{
	t_vect				*vector;

	if (!(vector = (t_vect *)malloc(sizeof(t_vect) * 1)))
		return (NULL);
	vector->x = x;
	vector->y = y;
	vector->z = z;
	return (vector);
}

t_color					*ft_init_color(int c1, int c2, int c3)
{
	t_color				*color;

	if (!(color = (t_color *)malloc(sizeof(t_color) * 1)))
		return (NULL);
	color->r = c1;
	color->g = c2;
	color->b = c3;
	color->tot = c1 + c2 + c3;
	return (color);
}

t_size_image			*ft_init_size(int a, int b)
{
	t_size_image		*size_image;

	if (!(size_image = (t_size_image *)malloc(sizeof(t_size_image) * 1)))
		return (NULL);
	size_image->x = a;
	size_image->y = b;
	return (size_image);
}

t_ambiant_light			*ft_init_light(float r, t_color *amb_color)
{
	t_ambiant_light		*amb_light;

	if (!(amb_light = (t_ambiant_light *)malloc(sizeof(t_ambiant_light) * 1)))
		return (NULL);
	amb_light->r = r;
	amb_light->color = amb_color;
	return (amb_light);
}
