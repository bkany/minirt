/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_param.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bkany <bkany@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/26 13:37:07 by bkany             #+#    #+#             */
/*   Updated: 2020/07/26 13:44:13 by bkany            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../minirt.h"

int		ft_check_vect(t_vect *v)
{
	if ((v->x < -1 || v->x > 1) || (v->y < -1 || v->y > 1) ||
		(v->z < -1 || v->z > 1))
		return (0);
	else
		return (1);
}

int		ft_check_color(t_color *c)
{
	if ((c->r < 0 || c->r > 255) || (c->g < 0 || c->g > 255) ||
		(c->b < 0 || c->b > 255))
		return (0);
	else
		return (1);
}

int		ft_check_ratio(float r)
{
	if (r < 0 || r > 1)
		return (0);
	else
		return (1);
}

int		ft_check_fov(int fov)
{
	if (fov > 180 || fov < 0)
		return (0);
	else
		return (1);
}

int		ft_check_positive(float f)
{
	if (f >= 0)
		return (1);
	else
		return (0);
}
