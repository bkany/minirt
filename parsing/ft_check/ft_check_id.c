/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_id.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bkany <bkany@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/26 13:37:07 by bkany             #+#    #+#             */
/*   Updated: 2020/06/26 13:14:13 by bkany            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../minirt.h"

static char		**ft_keywords(void)
{
	char		**tabkey;

	if (!(tabkey = (char **)malloc(sizeof(char *) * 12)))
		return (NULL);
	tabkey[0] = ft_strdup("R");
	tabkey[1] = ft_strdup("A");
	tabkey[2] = ft_strdup("c");
	tabkey[3] = ft_strdup("l");
	tabkey[4] = ft_strdup("sp");
	tabkey[5] = ft_strdup("pl");
	tabkey[6] = ft_strdup("sq");
	tabkey[7] = ft_strdup("cy");
	tabkey[8] = ft_strdup("tr");
	tabkey[9] = ft_strdup("cu");
	tabkey[10] = ft_strdup("py");
	tabkey[11] = NULL;
	return (tabkey);
}

static char		*ft_free_and_return(char **tab, char **tabkey)
{
	ft_free_tabtab(tabkey);
	return (tab[0]);
}

char			*ft_check_id(char **tab)
{
	char		**tabkey;
	int			i;
	int			len_id;
	int			j;

	tabkey = ft_keywords();
	i = 0;
	j = 0;
	len_id = ft_strlen(tab[0]);
	while (tabkey[i] != NULL)
	{
		if (ft_strncmp(tab[0], tabkey[i], len_id) == 0)
			return (ft_free_and_return(tab, tabkey));
		else
			i++;
	}
	if (tabkey[i] == NULL)
		return (ft_free_and_return(tab, tabkey));
	else
		return (NULL);
}

int				ft_check_id2(char *tab)
{
	char		**tabkey;
	int			i;
	int			len_id;
	int			j;

	tabkey = ft_keywords();
	i = 0;
	j = 0;
	len_id = ft_strlen(tab);
	while (tabkey[i] != NULL)
	{
		if (ft_strncmp(tab, tabkey[i], len_id) == 0)
		{
			ft_free_tabtab(tabkey);
			return (1);
		}
		else
			i++;
	}
	if (tabkey[i] == NULL)
	{
		ft_free_tabtab(tabkey);
		return (0);
	}
	return (0);
}
