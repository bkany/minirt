/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fill_set_1.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bkany <bkany@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/26 13:37:07 by bkany             #+#    #+#             */
/*   Updated: 2020/07/26 13:44:13 by bkany            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../minirt.h"

int			ft_fill_set_size(t_set *set, char **result)
{
	if (!(set->win))
	{
		set->win = ft_fill_size(result);
		if (set->win != NULL)
			return (1);
		else
			return (0);
	}
	else
	{
		ft_printf("Erreur - Ecran deja parametre\n");
		return (0);
	}
}

int			ft_fill_set_amb_light(t_set *set, char **result)
{
	int		error;

	error = 1;
	if (!(set->light1))
	{
		set->light1 = ft_fill_amb_light(result);
		if (set->light1 != NULL)
		{
			error = ft_check_ratio(set->light1->r) *
				ft_check_color(set->light1->color);
			if (error == 0)
				ft_printf("Erreur - Mauvais parametrage lumiere\n");
		}
		else
		{
			ft_printf("Erreur - Mauvais parametrage lumiere\n");
			error = 0;
		}
	}
	else
	{
		error = 0;
		ft_printf("Erreur - Lumiere deja parametree\n");
	}
	return (error);
}

int			ft_fill_set_cam(t_set *set, char **result)
{
	int		error;

	error = 1;
	if (ft_strncmp(ft_check_id(result), "c", 2) == 0 && !(set->cam))
	{
		set->cam = ft_fill_cam(result);
		if (set->cam != NULL)
			error = ft_check_vect(set->cam->vect) * ft_check_fov(set->cam->fov);
		else
			error = 0;
		if (error == 0)
			ft_printf("Erreur - Mauvais parametrage camera\n");
	}
	else if (ft_strncmp(ft_check_id(result), "c", 2) == 0 && (set->cam))
	{
		error = ft_fill_cams(set->cam, result);
		if (error == 0)
			ft_printf("Erreur - Mauvais parametrage camera\n");
	}
	return (error);
}

int			ft_fill_set_lights(t_set *set, char **result)
{
	int		error;

	error = 1;
	if (ft_strncmp(ft_check_id(result), "l", 2) == 0 && !(set->light2))
	{
		set->light2 = ft_fill_light(result);
		if (set->light2 != NULL)
		{
			error = ft_check_ratio(set->light2->range) *
				ft_check_color(set->light2->color);
		}
		else
			error = 0;
		if (error == 0)
			ft_printf("Erreur - Mauvais parametrage lumiere supplementaire\n");
	}
	else if (ft_strncmp(ft_check_id(result), "l", 2) == 0 && (set->light2))
	{
		error = ft_fill_lights(set->light2, result);
		if (error == 0)
		{
			ft_printf("Erreur - Mauvais parametrage ");
			ft_printf("lumieres supplementaires\n");
		}
	}
	return (error);
}

int			ft_fill_set_sphere(t_set *set, char **result)
{
	int		error;

	error = 1;
	if (ft_strncmp(ft_check_id(result), "sp", 2) == 0 && !(set->fig))
	{
		set->fig = ft_init_fig();
		if (set->fig != NULL)
		{
			set->fig->sphere = ft_fill_sphere(result);
			if (set->fig->sphere != NULL)
			{
				error = ft_check_color(set->fig->sphere->color) *
					ft_check_positive(set->fig->sphere->diam);
			}
			else
				error = 0;
		}
		else
			error = 0;
		if (error == 0)
			ft_printf("Erreur - Mauvais parametrage sphere\n");
	}
	else if (ft_strncmp(ft_check_id(result), "sp", 2) == 0 && (set->fig))
	{
		if (!(set->fig->sphere))
		{
			set->fig->sphere = ft_fill_sphere(result);
			if (set->fig->sphere != NULL)
			{
				error = ft_check_color(set->fig->sphere->color) *
					ft_check_positive(set->fig->sphere->diam);
			}
			else
				error = 0;
			if (error == 0)
				ft_printf("Erreur - Mauvais parametrage sphere\n");
		}
		else if ((error = ft_fill_spheres(set->fig->sphere, result)) == 0)
		{
			ft_printf("Erreur - Mauvais parametrage");
			ft_printf(" sphere supplementaire\n");
		}
	}
	return (error);
}

int			ft_fill_set_plan(t_set *set, char **result)
{
	int		error;

	error = 1;
	if (ft_strncmp(ft_check_id(result), "pl", 2) == 0 && !(set->fig))
	{
		set->fig = ft_init_fig();
		if (set->fig != NULL)
		{
			set->fig->plan = ft_fill_plan(result);
			if (set->fig->plan != NULL)
			{
				error = ft_check_vect(set->fig->plan->vect) *
					ft_check_color(set->fig->plan->color);
			}
			else
				error = 0;
		}
		else
			error = 0;
		if (error == 0)
			ft_printf("Erreur - Mauvais parametrage plan\n");
	}
	else if (ft_strncmp(ft_check_id(result), "pl", 2) == 0 && (set->fig))
	{
		if (!(set->fig->plan))
		{
			set->fig->plan = ft_fill_plan(result);
			if (set->fig->plan != NULL)
			{
				error = ft_check_vect(set->fig->plan->vect) *
					ft_check_color(set->fig->plan->color);
			}
			else
				error = 0;
			if (error == 0)
				ft_printf("Erreur - Mauvais parametrage plan\n");
		}
		else
		{
			ft_fill_plans(set->fig->plan, result);
			if (error == 0)
				ft_printf("Erreur - Mauvais parametrage plan supplementaire\n");
		}
	}
	return (error);
}

int			ft_fill_set_square(t_set *set, char **result)
{
	int		error;

	error = 1;
	if (ft_strncmp(ft_check_id(result), "sq", 2) == 0 && !(set->fig))
	{
		set->fig = ft_init_fig();
		if (set->fig != NULL)
		{
			set->fig->square = ft_fill_square(result);
			if (set->fig->square != NULL)
			{
				error = ft_check_vect(set->fig->square->vect)
					* ft_check_positive(set->fig->square->h)
					* ft_check_color(set->fig->square->color);
			}
			else
				error = 0;
		}
		else
			error = 0;
		if (error == 0)
			ft_printf("Erreur - Mauvais parametrage square\n");
	}
	else if (ft_strncmp(ft_check_id(result), "sq", 2) == 0 && (set->fig))
	{
		if (!(set->fig->square))
		{
			set->fig->square = ft_fill_square(result);
			if (set->fig->square != NULL)
			{
				error = ft_check_vect(set->fig->square->vect)
					* ft_check_positive(set->fig->square->h)
					* ft_check_color(set->fig->square->color);
			}
			else
				error = 0;
			if (error == 0)
				ft_printf("Erreur - Mauvais parametrage square\n");
		}
		else
		{
			error = ft_fill_squares(set->fig->square, result);
			if (error == 0)
			{
				ft_printf("Erreur - Mauvais parametrage ");
				ft_printf("square supplementaire\n");
			}
		}
	}
	return (error);
}

int			ft_fill_set_cyl(t_set *set, char **result)
{
	int		error;

	error = 1;
	if (ft_strncmp(ft_check_id(result), "cy", 2) == 0 && !(set->fig))
	{
		set->fig = ft_init_fig();
		if (set->fig != NULL)
		{
			set->fig->cyl = ft_fill_cyl(result);
			if (set->fig->cyl != NULL)
			{
				error = ft_check_vect(set->fig->cyl->vect) *
					ft_check_color(set->fig->cyl->color) *
					ft_check_positive(set->fig->cyl->diam) *
					ft_check_positive(set->fig->cyl->h);
			}
			else
				error = 0;
		}
		else
			error = 0;
		if (error == 0)
			ft_printf("Erreur - Mauvais parametrage cylindre");
	}
	else if (ft_strncmp(ft_check_id(result), "cy", 2) == 0 && (set->fig))
	{
		if (!(set->fig->cyl))
		{
			set->fig->cyl = ft_fill_cyl(result);
			if (set->fig->cyl != NULL)
			{
				error = ft_check_vect(set->fig->cyl->vect) *
					ft_check_color(set->fig->cyl->color) *
					ft_check_positive(set->fig->cyl->diam) *
					ft_check_positive(set->fig->cyl->h);
			}
			else
				error = 0;
			if (error == 0)
				ft_printf("Erreur - Mauvais parametrage cylindre");
		}
		else
		{
			error = ft_fill_cyls(set->fig->cyl, result);
			if (error == 0)
			{
				ft_printf("Erreur - Mauvais parametrage ");
				ft_printf("cylindre supplementaire\n");
			}
		}
	}
	return (error);
}

int			ft_fill_set_triang(t_set *set, char **result)
{
	int		error;

	error = 1;
	if (ft_strncmp(ft_check_id(result), "tr", 2) == 0 && !(set->fig))
	{
		set->fig = ft_init_fig();
		if (set->fig != NULL)
		{
			set->fig->triang = ft_fill_triang(result);
			if (set->fig->triang != NULL)
			{
				error = ft_check_color(set->fig->triang->color);
			}
			else
				error = 0;
		}
		else
			error = 0;
		if (error == 0)
			ft_printf("Erreur - Mauvais parametrage triangle");
	}
	else if (ft_strncmp(ft_check_id(result), "tr", 2) == 0 && (set->fig))
	{
		if (!(set->fig->triang))
		{
			set->fig->triang = ft_fill_triang(result);
			if (set->fig->triang != NULL)
				error = ft_check_color(set->fig->triang->color);
			else
				error = 0;
			if (error == 0)
				ft_printf("Erreur - Mauvais parametrage triangle");
		}
		else
		{
			error = ft_fill_triangs(set->fig->triang, result);
			if (error == 0)
			{
				ft_printf("Erreur - Mauvais parametrage ");
				ft_printf("triangle supplementaire\n");
			}
		}
	}
	return (error);
}
