/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fill_set.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bkany <bkany@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/26 13:37:07 by bkany             #+#    #+#             */
/*   Updated: 2020/07/26 13:44:13 by bkany            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../minirt.h"

static int	ft_fill_set_set(t_set *set, char **tab_line)
{
	int	error2;

	error2 = 0;
	if (ft_strncmp(tab_line[0], "R", 2) == 0)
		error2 = ft_fill_set_size(set, tab_line);
	else if (ft_strncmp(tab_line[0], "A", 2) == 0)
		error2 = ft_fill_set_amb_light(set, tab_line);
	else if (ft_strncmp(tab_line[0], "c", 2) == 0)
		error2 = ft_fill_set_cam(set, tab_line);
	else if (ft_strncmp(tab_line[0], "l", 2) == 0)
		error2 = ft_fill_set_lights(set, tab_line);
	else if (ft_strncmp(tab_line[0], "sp", 2) == 0)
		error2 = ft_fill_set_sphere(set, tab_line);
	else if (ft_strncmp(tab_line[0], "pl", 2) == 0)
		error2 = ft_fill_set_plan(set, tab_line);
	else if (ft_strncmp(tab_line[0], "sq", 2) == 0)
		error2 = ft_fill_set_square(set, tab_line);
	else if (ft_strncmp(tab_line[0], "cy", 2) == 0)
		error2 = ft_fill_set_cyl(set, tab_line);
	else if (ft_strncmp(tab_line[0], "tr", 2) == 0)
		error2 = ft_fill_set_triang(set, tab_line);
	return (error2);
}

static int	ft_read_file_and_fill(char *file_name, t_set *set, int error2)
{
	int		fd;
	int		error;
	char	*line;
	char	**tab_line;

	error = 1;
	tab_line = NULL;
	fd = open(file_name, O_RDONLY);
	while ((error = get_next_line(fd, &line)) > 0 && error2 == 1)
	{
		tab_line = ft_split_ws(line);
		if (tab_line[0] != NULL && ft_check_id2(tab_line[0]) == 1)
			error2 = ft_fill_set_set(set, tab_line);
		else if (tab_line[0] != NULL && ft_check_id2(tab_line[0]) == 0)
			error2 = 0;
		else if (tab_line == NULL)
			error2 = 0;
		ft_free_tabtab(tab_line);
	}
	if (fd == -1)
		error2 = -1;
	free(line);
	close(fd);
	return (error2);
}

t_set		*ft_fill_set(char *file_name)
{
	int		error2;
	t_set	*set;

	set = NULL;
	if ((error2 = ft_check_file_name(file_name)) == 1)
	{
		set = ft_init_set();
		error2 = ft_read_file_and_fill(file_name, set, error2);
		if (error2 == -1)
		{
			ft_printf("Fichier inexistant - Fin du programme\n");
		}
		if (error2 == 0 || error2 == 1)
		{
			if (set->win == NULL )
			{
				ft_printf("Erreur - Mauvais parametrage de l'image\n");
				error2 = 0;
			}
			if (set->light1 == NULL && error2 != 0)
			{
				ft_printf("Erreur - Mauvais parametrage de la lumiere\n");
				error2 = 0;
			}
			if (set->cam == NULL && error2 != 0)
			{
				ft_printf("Erreur - Mauvais parametrage de camera\n");
				error2 = 0;
			}
		}
		if (error2 == 0 || error2 == -1)
		{
			ft_free_set(set);
			ft_printf("Echec du parametrage - Fin du programme\n");
		}
		return (set);
	}
	else if (error2 == 0)
	{
		ft_free_set(set);
		ft_printf("Mauvais nom de fichier passe en parametre - Fin du programme\n");
		return (NULL);
	}
	else if (error2 == -1)
	{
		ft_free_set(set);
		ft_printf("Fichier inexistant - Fin du programme\n");
		return (NULL);
	}
	else
		return (NULL);
}
