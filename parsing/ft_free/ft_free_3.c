/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_free_1.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bkany <bkany@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/26 13:37:07 by bkany             #+#    #+#             */
/*   Updated: 2020/06/26 13:14:13 by bkany            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../minirt.h"

void		ft_free_fig(t_fig *fig)
{
	if (fig != NULL)
	{
		ft_free_sphere(fig->sphere);
		ft_free_plan(fig->plan);
		ft_free_square(fig->square);
		ft_free_cyl(fig->cyl);
		ft_free_triang(fig->triang);
		free(fig);
		fig = NULL;
	}
}

void		ft_free_set(t_set *set)
{
	if (set != NULL)
	{
		if (set->win != NULL)
			free(set->win);
		ft_free_amb_light(set->light1);
		ft_free_camera(set->cam);
		ft_free_lights(set->light2);
		ft_free_fig(set->fig);
		free(set);
		set = NULL;
	}
}

void		ft_free_tabtab(char **tab)
{
	int				i;

	i = 0;
	if (tab != NULL)
	{
		if (tab[0] != NULL)
		{
			while (tab[i] != NULL)
			{
				free(tab[i]);
				i++;
			}
		}
		free(tab);
		tab = NULL;
	}
}
