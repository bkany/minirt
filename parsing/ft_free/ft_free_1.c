/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_free_1.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bkany <bkany@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/26 13:37:07 by bkany             #+#    #+#             */
/*   Updated: 2020/06/26 13:14:13 by bkany            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../minirt.h"

void		ft_free_amb_light(t_ambiant_light *amb_light)
{
	if (amb_light != NULL)
	{
		free(amb_light->color);
		free(amb_light);
		amb_light = NULL;
	}
}

void		ft_free_camera(t_camera *cam)
{
	t_camera		*tmp;
	t_camera		*tmp2;

	tmp = NULL;
	tmp2 = NULL;
	if (cam != NULL)
	{
		tmp = cam;
		while (tmp)
		{
			tmp2 = tmp->next;
			free(tmp->coord);
			free(tmp->vect);
			tmp->next = NULL;
			free(tmp);
			tmp = tmp2;
		}
		free(cam);
		cam = NULL;
	}
}

void		ft_free_lights(t_plus_light *lights)
{
	t_plus_light	*tmp;
	t_plus_light	*tmp2;

	tmp = NULL;
	tmp2 = NULL;
	if (lights != NULL)
	{
		tmp = lights;
		while (tmp)
		{
			tmp2 = tmp->next;
			free(tmp->coord);
			free(tmp->color);
			tmp->next = NULL;
			free(tmp);
			tmp = tmp2;
		}
		free(lights);
		lights = NULL;
	}
}

void		ft_free_sphere(t_sphere *sphere)
{
	t_sphere		*tmp;
	t_sphere		*tmp2;

	tmp = NULL;
	tmp2 = NULL;
	if (sphere != NULL)
	{
		tmp = sphere;
		while (tmp)
		{
			tmp2 = tmp->next;
			free(tmp->coord);
			free(tmp->color);
			tmp->next = NULL;
			free(tmp);
			tmp = tmp2;
		}
		free(sphere);
		sphere = NULL;
	}
}

void		ft_free_plan(t_plan *plan)
{
	t_plan			*tmp;
	t_plan			*tmp2;

	tmp = NULL;
	tmp2 = NULL;
	if (plan != NULL)
	{
		tmp = plan;
		while (tmp)
		{
			tmp2 = tmp->next;
			free(tmp->coord);
			free(tmp->vect);
			free(tmp->color);
			tmp->next = NULL;
			free(tmp);
			tmp = tmp2;
		}
		free(plan);
		plan = NULL;
	}
}
