/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_free_1.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bkany <bkany@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/26 13:37:07 by bkany             #+#    #+#             */
/*   Updated: 2020/06/26 13:14:13 by bkany            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../minirt.h"

void		ft_free_square(t_square *square)
{
	t_square		*tmp;
	t_square		*tmp2;

	tmp = NULL;
	tmp2 = NULL;
	if (square != NULL)
	{
		tmp = square;
		while (tmp)
		{
			tmp2 = tmp->next;
			free(tmp->coord);
			free(tmp->vect);
			free(tmp->color);
			tmp->next = NULL;
			free(tmp);
			tmp = tmp2;
		}
		free(square);
		square = NULL;
	}
}

void		ft_free_cyl(t_cyl *cyl)
{
	t_cyl			*tmp;
	t_cyl			*tmp2;

	tmp = NULL;
	tmp2 = NULL;
	if (cyl != NULL)
	{
		tmp = cyl;
		while (tmp)
		{
			tmp2 = tmp->next;
			free(tmp->coord);
			free(tmp->vect);
			free(tmp->color);
			tmp->next = NULL;
			free(tmp);
			tmp = tmp2;
		}
		free(cyl);
		cyl = NULL;
	}
}

void		ft_free_triang(t_triang *triang)
{
	t_triang		*tmp;
	t_triang		*tmp2;

	tmp = NULL;
	tmp2 = NULL;
	if (triang != NULL)
	{
		tmp = triang;
		while (tmp)
		{
			tmp2 = tmp->next;
			free(tmp->coord1);
			free(tmp->coord2);
			free(tmp->coord3);
			free(tmp->color);
			tmp->next = NULL;
			free(tmp);
			tmp = tmp2;
		}
		free(triang);
		triang = NULL;
	}
}
