NAME		=	miniRT
FLAGS		=	-Wall -Werror -Wextra -Weverything
MLX			=	-lmlx -lXext -lX11 -lm
LIBS		=	-L ./tools/libft -lft -L ./print/ft_printf -lftprintf -L /usr/local/lib
INCLUDES	=	-I ./tools/libft -I ./print/ft_printf/include -I ./parsing/get_next_line -I /usr/local/include
SRCS		=	parsing/ft_init/ft_init_1.c\
			parsing/ft_init/ft_init_2.c\
			parsing/ft_init/ft_init_3.c\
			parsing/ft_init/ft_init_4.c\
			parsing/ft_parsing/ft_parsing_1_size_light_cam.c\
			parsing/ft_parsing/ft_parsing_2_lights.c\
			parsing/ft_parsing/ft_parsing_3_spheres.c\
			parsing/ft_parsing/ft_parsing_4_plans.c\
			parsing/ft_parsing/ft_parsing_5_squares.c\
			parsing/ft_parsing/ft_parsing_6_cylindres.c\
			parsing/ft_parsing/ft_parsing_7_triangles.c\
			parsing/ft_free/ft_free_1.c\
			parsing/ft_free/ft_free_2.c\
			parsing/ft_free/ft_free_3.c\
			parsing/ft_fill_set/ft_fill_set_1.c\
			parsing/ft_fill_set/ft_fill_set.c\
			parsing/ft_check/ft_check_param.c\
			parsing/ft_check/ft_check_id.c\
			parsing/ft_check/ft_check_file_name.c\
			print/print_struct_1.c\
			print/print_struct_2.c\
			print/print_struct_3.c\
			print/print_struct_4.c\
			raytracing/ft_tools.c\
			raytracing/ft_tools2.c\
			raytracing/ft_intersect.c\
			raytracing/ft_get_color.c\
			raytracing/ft_inshadow.c\
			show/ft_show.c\
			print/ft_printf/ft_printf.c\
			parsing/get_next_line/get_next_line.c\
			parsing/get_next_line/get_next_line_utils.c\
			main.c
OBJS		= 	$(SRCS:.c=.o)

$(%.o): $(%.c)
	clang $(FLAGS) $(INCLUDES) -c $< -o $(<:.c=.o)

all: make-libft make-printf $(NAME)

make-libft:
	make -C tools/libft/

make-printf:
	make -C print/ft_printf/  

$(NAME) : $(OBJS)
	clang $(FLAGS) $(INCLUDES) $(OBJS) $(LIBS) $(MLX) -o $(NAME)

	
clean:
			make -C tools/libft clean
			make -C print/ft_printf clean
			rm -f $(OBJS)

fclean:
			make -C tools/libft fclean
			make -C print/ft_printf fclean
			rm -f $(OBJS)
			rm -f $(NAME)

re:			fclean all
