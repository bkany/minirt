/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minirt.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bkany <bkany@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/25 13:37:07 by bkany             #+#    #+#             */
/*   Updated: 2020/06/25 13:14:13 by bkany            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBMINIRT_H
# define LIBMINIRT_H

# include "tools/libft/libft.h"
# include "print/ft_printf/include/libftprintf.h"
# include "parsing/get_next_line/get_next_line.h"
# include <stdlib.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <fcntl.h>
# include <mlx.h>
# include <math.h>

# define PARAM 		11

/*
** STRUCTURES
*/
typedef struct			s_coord
{
	float				x;
	float				y;
	float				z;
}						t_coord;

typedef struct			s_vect
{
	float				x;
	float				y;
	float				z;
}						t_vect;

typedef struct			s_color
{
	int					r;
	int					g;
	int					b;
	int					tot;
}						t_color;

typedef struct			s_size_image
{
	int					x;
	int					y;
}						t_size_image;

typedef struct			s_ambiant_light
{
	float				r;
	t_color				*color;
}						t_ambiant_light;

typedef struct			s_camera
{
	t_coord				*coord;
	t_vect				*vect;
	int					fov;
	struct s_camera		*next;
}						t_camera;

typedef struct			s_plus_light
{
	t_coord				*coord;
	float				range;
	t_color				*color;
	struct s_plus_light	*next;
}						t_plus_light;

typedef struct			s_sphere
{
	t_coord				*coord;
	float				diam;
	t_color				*color;
	struct s_sphere		*next;
}						t_sphere;

typedef struct			s_plan
{
	t_coord				*coord;
	t_vect				*vect;
	t_color				*color;
	struct s_plan		*next;
}						t_plan;

typedef struct			s_square
{
	t_coord				*coord;
	t_vect				*vect;
	float				h;
	t_color				*color;
	struct s_square		*next;
}						t_square;

typedef struct			s_cyl
{
	t_coord				*coord;
	t_vect				*vect;
	float				diam;
	float				h;
	t_color				*color;
	struct s_cyl		*next;
}						t_cyl;

typedef struct			s_triang
{
	t_coord				*coord1;
	t_coord				*coord2;
	t_coord				*coord3;
	t_color				*color;
	struct s_triang		*next;
}						t_triang;

typedef struct			s_fig
{
	t_sphere			*sphere;
	t_plan				*plan;
	t_square			*square;
	t_cyl				*cyl;
	t_triang			*triang;
}						t_fig;

typedef struct			s_set
{
	t_size_image		*win;
	t_ambiant_light		*light1;
	t_camera			*cam;
	t_plus_light		*light2;
	t_fig				*fig;
}						t_set;

typedef struct			s_solve
{
	t_coord				*inter;
	t_vect				*normale;
	float				t1;
	float				t2;
	int					type;
	int					num;
}						t_solve;

/*
**	type =	0 --> sphere
**		1 --> plan
**		2 --> square
**		3 --> cyl
**		4 --> triang
**
**	num =	0 --> 1rst
**		1 --> 2nd ...
*/

typedef struct			s_data
{
	void				*img;
	char				*addr;
	int					bits_per_pixel;
	int					line_length;
	int					endian;
}						t_data;
/*
** INITIALISATION DES STRUCTURES
*/
t_coord					*ft_init_coord(float x, float y, float z);
t_vect					*ft_init_vect(float x, float y, float z);
t_color					*ft_init_color(int c1, int c2, int c3);
t_size_image			*ft_init_size(int a, int b);
t_ambiant_light			*ft_init_light(float r, t_color *amb_color);
t_camera				*ft_init_cam(t_coord *pov, t_vect *vect_o, int fov);
t_plus_light			*ft_init_light2(t_coord *lum, float r, t_color *col);
t_sphere				*ft_init_sphere(t_coord *center, float d, t_color *col);
t_plan					*ft_init_plan(t_coord *axe, t_vect *v, t_color *col);
t_square				*ft_init_squ(t_coord *pt, t_vect *v, float l, t_color *col);
t_cyl					*ft_init_cyl1(t_coord *pt, t_vect *v, float d);
void					ft_init_cyl2(t_cyl *cyl, float l, t_color *col);
t_triang				*ft_init_tri(t_coord *c1, t_coord *c2, t_coord *c3, t_color *col);
t_fig					*ft_init_fig(void);
t_set					*ft_init_set(void);

/*
** PRINT DES STRUCTURES
*/
void					ft_print_coord(t_coord *coord);
void					ft_print_vect(t_vect *vect);
void					ft_print_color(t_color *col);
void					ft_print_size_image(t_size_image *size);
void					ft_print_ambiant_light(t_ambiant_light *light);
void					ft_print_camera(t_camera *cam);
void					ft_print_lights(t_plus_light *lights);
void					ft_print_sphere(t_sphere *sphere);
void					ft_print_plan(t_plan *plan);
void					ft_print_square(t_square *square);
void					ft_print_cyl(t_cyl *cyl);
void					ft_print_triang(t_triang *triang);
void					ft_print_fig(t_fig *fig);
void					ft_print_set(t_set *set);

/*
** FREE DES STRUCTURES
*/
void					ft_free_amb_light(t_ambiant_light *amb_light);
void					ft_free_camera(t_camera *cam);
void					ft_free_lights(t_plus_light *lights);
void					ft_free_sphere(t_sphere *sphere);
void					ft_free_plan(t_plan *plan);
void					ft_free_square(t_square *square);
void					ft_free_cyl(t_cyl *cyl);
void					ft_free_triang(t_triang *triang);
void					ft_free_fig(t_fig *fig);
void					ft_free_set(t_set *set);
void					ft_free_tabtab(char **tab);

/*
** CHECK DES PARAMETRES
*/
char					*ft_check_id(char **tab);
int						ft_check_id2(char *tab);
int						ft_check_vect(t_vect *v);
int						ft_check_color(t_color *c);
int						ft_check_ratio(float r);
int						ft_check_fov(int fov);
int						ft_check_positive(float f);
int						ft_check_file_name(char *name);

/*
** PARSING DU FICHIER RT
*/
t_size_image			*ft_fill_size(char **result);
t_ambiant_light			*ft_fill_amb_light(char **result);
t_camera				*ft_fill_cam(char **result);
int						ft_fill_cams(t_camera *cam, char **result);
t_plus_light			*ft_fill_light(char **result);
int						ft_fill_lights(t_plus_light *lights, char **result);
t_sphere				*ft_fill_sphere(char **result);
int						ft_fill_spheres(t_sphere *sphere, char **result);
t_plan					*ft_fill_plan(char **result);
int						ft_fill_plans(t_plan *plan, char **result);
t_square				*ft_fill_square(char **result);
int						ft_fill_squares(t_square *square, char **result);
t_cyl					*ft_fill_cyl(char **result);
int						ft_fill_cyls(t_cyl *cyl, char **result);
t_triang				*ft_fill_triang(char **result);
int						ft_fill_triangs(t_triang *triang, char **result);

/*
** REMPLISSAGE DE LA STRUCTURE SET
*/
int						ft_fill_set_size(t_set *set, char **result);
int						ft_fill_set_amb_light(t_set *set, char **result);
int						ft_fill_set_cam(t_set *set, char **result);
int						ft_fill_set_lights(t_set *set, char **result);
int						ft_fill_set_sphere(t_set *set, char **result);
int						ft_fill_set_plan(t_set *set, char **result);
int						ft_fill_set_square(t_set *set, char **result);
int						ft_fill_set_cyl(t_set *set, char **result);
int						ft_fill_set_triang(t_set *set, char **result);
t_set					*ft_fill_set(char *file_name);

/*
** TOOLS RAYTRACING
*/
t_vect					*ft_vect(t_coord *coord1, t_coord *coord2);
void					ft_set_vect(t_vect *v, float a, float b, float c);
t_vect					*ft_prodvect(t_vect *vect1, t_vect *vect2);
float					ft_prodscal(t_vect *vect1, t_vect *vect2);
float					ft_norm(t_vect	*vect);
float					ft_norm2(t_vect	*vect);
t_vect					*ft_multvect(t_vect *vect, float a);
t_vect					*ft_add_vect(t_vect *vect1, t_vect *vect2);
t_vect					*ft_sumvect(t_vect *vect1, t_vect *vect2);
float					ft_dist(t_coord *coord1, t_coord *coord2);
float					ft_dist2(t_coord *coord1, t_coord *coord2);
int						ft_isalign(t_coord *coord_a, t_coord *coord_b, t_coord *coord_c);
void					ft_vectunit(t_vect *vect);
t_vect					*ft_vectunit2(t_vect *vect);
float					ft_angle(t_vect *vect1, t_vect *vect2);
float					ft_abs(float f);
t_coord					*ft_transl_pt(t_coord *coord, t_vect *vect, float a);
t_vect					*ft_mirorvect(t_vect *i, t_vect *norm);

/*
** INTERSECTIONS
*/
t_solve					*ft_init_solve(void);
t_solve					*ft_fill_solve(t_coord *p, t_vect *n, float s1, float s2, int t, int nu);
void					ft_free_solve(t_solve *t);
t_solve					*ft_swipe_fig(t_set *set, t_coord *cam_coord, t_vect *vect_dir_win);
t_solve					*ft_inter_sphere(t_coord *cam_coord, t_sphere *sphere, t_vect *vect_dir_win);
t_solve					*ft_inter_plan(t_coord *cam_coord, t_plan *plan, t_vect *vect_dir_win);
t_solve					*ft_inter_cyl(t_coord *cam_coord, t_cyl *cyl, t_vect *vect_dir_win);
t_solve					*ft_inter_square(t_coord *cam_coord, t_square *square, t_vect *vect_dir_win);
t_solve					*ft_inter_triang(t_coord *cam_coord, t_triang *triang, t_vect *vect_dir_win);

/*
** GET COLORS
*/

int						ft_get_color_sphere_fig(t_color *col, t_solve *solve, t_set *set);
int						ft_get_color(t_solve *solve, t_set *set);
int						ft_inshadow(t_plus_light *l, t_set *set, t_solve *solve);
t_sphere				*ft_get_sphere(t_solve	*solve, t_set *set);
t_plan					*ft_get_plan(t_solve *solve, t_set *set);
t_square				*ft_get_square(t_solve *solve, t_set *set);
t_cyl					*ft_get_cyl(t_solve *solve, t_set *set);
t_triang				*ft_get_triang(t_solve *solve, t_set *set);
/*
** ROTATIONS
*/
t_vect					*ft_rotx_vect(t_vect *vect, float theta);
t_vect					*ft_roty_vect(t_vect *vect, float theta);
t_vect					*ft_rotz_vect(t_vect *vect, float theta);
t_coord					*ft_rotx_coord(t_coord *coord, float theta);
t_coord					*ft_roty_coord(t_coord *coord, float theta);
t_coord					*ft_rotz_coord(t_coord *coord, float theta);
void					ft_rot_lights(t_plus_light *light, float thetax, float thetay, float thetaz);
void					ft_rot_spheres(t_sphere *sphere, float thetax, float thetay, float thetaz);
void					ft_rot_plans(t_plan *plan, float thetax, float thetay, float thetaz);
void					ft_rot_squares(t_square *square, float thetax, float thetay, float thetaz);
void					ft_rot_cyls(t_cyl *cyl, float thetax, float thetay, float thetaz);
void					ft_rot_triangles(t_triang *triang, float thetax, float thetay, float thetaz);
void					ft_rot_fig(t_fig *fig, float thetax, float thetay, float thetaz);
void					ft_rot_set(t_set *set, t_vect *vect);
/*
** SHOW
*/
void					ft_fill_image(t_data data, t_set *set);

#endif
