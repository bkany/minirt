/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bkany <bkany@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/06 15:00:53 by bkany             #+#    #+#             */
/*   Updated: 2019/11/28 14:18:21 by bkany            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdio.h>

static int		ft_count_size(int n)
{
	int i;

	i = 1;
	if (n == -2147483648)
		return (10);
	while (n >= 10 || n <= -10)
	{
		i++;
		n = n / 10;
	}
	return (i);
}

char			*ft_itoa(int nb)
{
	int			size;
	char		*result;
	long int	n;

	n = nb;
	size = (n < 0) ? ft_count_size(n) + 1 : ft_count_size(n);
	if (!(result = (char *)malloc(sizeof(char) * (size + 1))))
		return (NULL);
	if (n < 0)
	{
		n = n * (-1);
		result[0] = '-';
	}
	result[size] = '\0';
	while (n >= 10)
	{
		result[size - 1] = n % 10 + '0';
		size--;
		n = n / 10;
	}
	result[size - 1] = n + '0';
	return (result);
}
